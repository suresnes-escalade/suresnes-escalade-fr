<?php
/*
 * This file is part of suresnes-escalade website
 */

declare(strict_types=1);

use Rector\CodeQuality\Rector\Class_\InlineConstructorDefaultToPropertyRector;
use Rector\Config\RectorConfig;
use Rector\Doctrine\Set\DoctrineSetList;
use Rector\Php80\Rector\Class_\ClassPropertyAssignToConstructorPromotionRector;
use Rector\Set\ValueObject\LevelSetList;
use Rector\Symfony\CodeQuality\Rector\ClassMethod\ActionSuffixRemoverRector;
use Rector\Symfony\Set\SymfonySetList;

return RectorConfig::configure()
    ->withSkip([
        __DIR__ . '/config/bundles.php',
        __DIR__ . '/public/index.php',
        __DIR__ . '/src/Security/PasswordHash.php',
        ActionSuffixRemoverRector::class => [
            __DIR__ . '/src/Controller/Admin/',
        ],
    ])

    ->withPaths([
        __DIR__ . '/config',
        __DIR__ . '/public',
        __DIR__ . '/src',
        __DIR__ . '/tests',
        __DIR__ . '/tools',
    ])

    ->withImportNames()

    ->withSets([
        SymfonySetList::SYMFONY_71,
        SymfonySetList::SYMFONY_CODE_QUALITY,
        SymfonySetList::SYMFONY_CONSTRUCTOR_INJECTION,
        LevelSetList::UP_TO_PHP_83,
        SymfonySetList::ANNOTATIONS_TO_ATTRIBUTES,
        DoctrineSetList::ANNOTATIONS_TO_ATTRIBUTES,
        DoctrineSetList::DOCTRINE_CODE_QUALITY,
    ])

    ->withRules([
        ClassPropertyAssignToConstructorPromotionRector::class,
        InlineConstructorDefaultToPropertyRector::class,
    ])

    ->withPhpSets()

    ->withPreparedSets(
        deadCode: true,
        codeQuality: true,
        // naming: true,
        privatization: true
    )

    ->withTypeCoverageLevel(0);
