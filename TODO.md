Packages à migrer/supprimer :

Package sensio/framework-extra-bundle is abandoned, you should avoid using it. Use Symfony instead.
Package sonata-project/cache is abandoned, you should avoid using it. No replacement was suggested.
Package swiftmailer/swiftmailer is abandoned, you should avoid using it. Use symfony/mailer instead.
Package symfony/debug is abandoned, you should avoid using it. Use symfony/error-handler instead.
Package symfony/inflector is abandoned, you should avoid using it. Use EnglishInflector from the String component instead.
Package symfony/swiftmailer-bundle is abandoned, you should avoid using it. Use symfony/mailer instead.
Package behat/symfony2-extension is abandoned, you should avoid using it. Use friends-of-behat/symfony-extension instead.
Package php-cs-fixer/diff is abandoned, you should avoid using it. No replacement was suggested.
