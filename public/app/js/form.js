const ready = (callback) => {
  if (document.readyState != "loading") {
    callback();
  } else {
    document.addEventListener("DOMContentLoaded", callback);
  }
};

ready(() => {
  if (refDate === undefined) {
    return;
  }

  const parent_block = document.getElementById("form_parent");
  const birthdayField = document.getElementById(
    "app_appbundle_subscription_birthday"
  );

  const showOrHideParentBlock = function (date, refDate) {
    const birthdayDate = new Date(date).getTime();

    if (birthdayDate > refDate) {
      parent_block.style.display = "block";
      parent_block.querySelector("select").setAttribute("required", true);
    } else {
      parent_block.style.display = "none";
      parent_block.querySelector("select").setAttribute("required", false);
    }
  };

  if (birthdayField.value) {
    showOrHideParentBlock(birthdayField.value, refDate);
  }

  birthdayField.addEventListener("change", (event) => {
    showOrHideParentBlock(event.target.value, refDate);
  });
});
