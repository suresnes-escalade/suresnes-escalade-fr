# Partie admin du site Escalade de Suresnes

Le projet utilise [symfony](https://symfony.com/) et [Sonata](https://sonata-project.org/)

[![pipeline status](https://gitlab.com/suresnes-escalade/suresnes-escalade-fr/badges/develop/pipeline.svg)](https://gitlab.com/suresnes-escalade/suresnes-escalade-fr/-/jobs)
[![PHPSTan level](https://img.shields.io/badge/PHPStan-level%208-brightgreen.svg?style=flat)]()

## Installation

Après avoir cloné ou mis à jour les sources, dans le répertoire correspondant, il faut récupérer (ou mettre à jour les dépendances) :

```sh
$ composer install --no-dev
```

Il faut mettre à jour les fichiers js et css :

```sh
$ ./app/console assets:install
```

## Tests

Pour lancer les tests qui utilisent [Behat](https://behat.org), taper la commande suivante :

```sh
$ ./bin/behat
```

Les tests sont exécutés automatiquement à chaque commit par [une pipeline Gitlab](https://gitlab.com/suresnes-escalade/suresnes-escalade-fr/-/jobs)

## Code

Une analyse statique du code est faite en utilisant [PHPStan](https://github.com/phpstan/phpstan) :

```sh
$ composer phpstan
```

L'analyse est pour le moment faite avec le niveau 8 mais l'idée est d'augmenter progressivement le niveau pour détecter de plus en plus d'éventuelles erreurs.

Le formatage du code est assuré par [PHP CS Fixer](https://github.com/FriendsOfPHP/PHP-CS-Fixer) en suivant les règles du [fichier de configuration](.php_cs.dist). Le but est de se conformer au standard [PSR-2](https://www.php-fig.org/psr/psr-2/)
