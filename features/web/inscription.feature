Feature: Subscription
  In order to do climbing
  As a user
  I need to be able to register

  Background: init
    Given a season:
      | title       | beginAt    | endAt      |
      | saison 2024 | 2024-05-15 | 2025-05-31 |
      | saison 2023 | 2023-05-15 | 2024-05-31 |
      | saison 2022 | 2022-05-15 | 2023-05-31 |

    Given some slots:
      | title        | season      | dayOfWeek | beginTime | endTime  | maxPlaces | marginPlaces | is_full |
      | lundi soir   | saison 2024 | 1         | 21:00:00  | 22:00:00 | 20        | 2            | 0       |
      | mardi soir   | saison 2024 | 2         | 21:30:00  | 22:30:00 | 10        | 5            | 0       |
      | mercredi 10h | saison 2024 | 3         | 10:00:00  | 11:00:00 | 10        | 5            | 0       |

    Given some subscriptions:
      | last_name | first_name | birthday   | season      | slot1        | slot2      | status |
      | Duchemin  | Marcel     | 10-09-1960 | saison 2024 | mercredi 10h | lundi soir | -1     |
      | Duchemin  | Gérard     | 02-04-1980 | saison 2024 | mercredi 10h | mardi soir | 0      |
      | Duchemin  | Maurice    | 01-06-1977 | saison 2024 | mercredi 10h | mardi soir | 2      |
      | Duchemin  | Alfred     | 07-08-2008 | saison 2024 | mercredi 10h | lundi soir | 2      |


  @mail
  Scenario: Register
    Given I am on route "simple_subscription"
    Then I should see "Inscription"
    And I should see "Coordonnées du grimpeur"
    When I select "Masculin" from "Sexe"
    When I fill in "Nom" with "Dupond"
    And I fill in "Prénom" with "Marcel"
    And I fill in "Date de naissance" with "1980-10-10"
    And I fill in "Lieu de naissance" with "Paris"
    And I fill in "Adresse" with "7 rue du Pont"
    And I fill in "Code postal" with "75002"
    And I fill in "Ville" with "Paris"
    And I fill in "Téléphone" with "0123456789"
    And I fill in "Adresse e-mail" with "marcel.dupond@gmail.com"
    And I select "lundi soir" from "1er souhait de créneau"
    And I select "mardi soir" from "2e souhait de créneau"
    And I press "Valider"
    Then I should see "Vos souhaits d'inscription ont bien été enregistrés pour la saison 2024"

    # Then I should see mail to "inscription@suresnes-escalade.fr"
    # And I should see mail to "marcel.dupond@gmail.com"

    When I go to route "slots_details"
    Then I should see "Marcel Dupond ( Attente paiement, certif non transmis )" in slot "lundi soir"

  Scenario: Remplissage créneaux
    Given I am on route "slots_details"
    Then I should see "Liste des inscrits par créneau / saison 2024"
    Then I should see "lundi soir"
    Then I should see "mardi soir"
    Then I should see "mercredi 10h"
    Then I should not see "Marcel Duchemin" in slot "mercredi 10h"
    Then I should see "Gérard Duchemin ( En attente, certif non transmis )" in slot "mercredi 10h"
    Then I should see "Maurice Duchemin ( Attente paiement, certif non transmis )" in slot "mercredi 10h"
    Then I should see "Alfred Duchemin ( Attente paiement, certif non transmis )" in slot "mercredi 10h"
