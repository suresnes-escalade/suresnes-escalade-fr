Feature: Administration
  In order to manager users and subscription
  As an admin
  I need to be able to connect to administration area

  Background: init
    Given a user:
      | user_login | user_pass | role          |
      | admin1     | pass1     | ADMINISTRATOR |
      | user5      | pass5     | AUTHOR        |

    Given a season:
      | title       | beginAt    | endAt      |
      | saison 2024 | 2024-05-15 | 2025-05-31 |
      | saison 2023 | 2023-05-15 | 2024-05-31 |
      | saison 2022 | 2022-05-15 | 2023-05-31 |

    Given some slots:
      | title        | season      | dayOfWeek | beginTime | endTime  | maxPlaces | marginPlaces | is_full |
      | lundi soir   | saison 2024 | 1         | 21:00:00  | 22:00:00 | 20        | 2            | 0       |
      | mardi soir   | saison 2024 | 2         | 21:30:00  | 22:30:00 | 10        | 5            | 0       |
      | mercredi 10h | saison 2024 | 3         | 10:00:00  | 11:00:00 | 10        | 5            | 0       |

    Given some subscriptions:
      | last_name | first_name | birthday   | birth_place | season      | slot1      | slot2        | status |
      | Dupond    | Marcel     | 10-10-1970 | Paris       | saison 2024 | lundi soir | mercredi 10h | 2      |
      | Martin    | Raymond    | 21-03-1981 | Grenoble    | saison 2024 | lundi soir | mercredi 10h | 1      |
      | Durand    | Micheline  | 11-09-1977 | Lyon        | saison 2024 | lundi soir | mercredi 10h | 1      |
      | Sannom    | Julie      | 19-04-1991 | Amiens      | saison 2024 | lundi soir | mercredi 10h | 1      |
      | Martin    | Raymonde   | 01-07-1983 | Paris       | saison 2024 | lundi soir | mercredi 10h | 1      |

  Scenario: Login as author
    Given I am authenticated with username "user5" and password "pass5"
    When I go to route "admin_subscription_list"
    Then I should not be allowed to see page

  Scenario: Login as admin
    Given I am authenticated with username "admin1" and password "pass1"
    When I go to route "sonata_admin_dashboard"
    Then I should see "Inscriptions"
    And I should see "Adhésions"
    And I should see "Saisons"
    And I should see "Créneaux"

  Scenario: Edit a subscription
    Given I am authenticated with username "admin1" and password "pass1"
    When I go to route "admin_subscription_list"
    And I should see "Dupond Marcel"
    And I follow "Dupond"
    Then I should see "Éditer \"Marcel Dupond (saison 2024)"

  Scenario: Add a slot
    Given I am authenticated with username "admin1" and password "pass1"
    When I go to route "admin_slot_list"
    Then I follow "Ajouter"
    And I select "saison 2024" from "Saison"
    And I fill in "Titre" with "vendredi midi"
    And I select "Vendredi" from "Jour de la semaine"
    And I fill in "Heure de début" with "10:00"
    And I fill in "Heure de fin" with "12:00"
    And I press "Créer"
    Then I should see "L'élément \"Saison 2024 / vendredi midi\" a été créé avec succès."

  Scenario: Change status to "Active"
    Given I am authenticated with username "admin1" and password "pass1"
    And I am on route "slots_details"
    Then I should see "Marcel Dupond ( Attente paiement, certif non transmis )" in slot "lundi soir"
    When I go to route "admin_subscription_list"
    And I follow "Dupond"
    Then I should see "Éditer \"Marcel Dupond (saison 2024)"
    And I select "Actif" from "Statut de l'inscription"
    And I select "mercredi 10h" from "Créneau validé"
    And I press "Mettre à jour"
    Then the response status code should be 200
    And the "Validé le" field should contain today
    And the "Validé par" field should contain "admin1"
    When I go to route "slots_details"
    Then I should see "Marcel Dupond ( Certif non transmis )" in slot "mercredi 10h"

  Scenario: Exporter la saison en cours
    Given I am authenticated with username "admin1" and password "pass1"
    When I go to route "admin_season_list"
    Then I click "Export vers FFME" for season "saison 2024"
    Then I should see in the header "content-type" "text/csv"
# Then the number of lines in the csv file should be 4
