Feature: Administration
  In order to manage payments for subscriptions
  As an admin
  I need to be able to see pending payments

  Scenario: New Payment notification
    Given I have the payload:
      """
      {
        "id": "11f67181a16ff4765aa18ea7062248cb",
        "date": "Sat Jun 27 16:52:28 CEST 2021",
        "amount": "171",
        "type": "Paiment en 3 fois",
        "url": "https://www.helloasso.com/associations/suresnes-escalade/adhesions/ado-et-adulte-paiement-en-3x",
        "payer_first_name": "Marcel",
        "payer_last_name": "Dupond",
        "url_receipt": "https://www.helloasso.com/associations/receipt/932bf585554",
        "url_tax_receipt": "https://www.helloasso.com/associations/taxt_receipt/932bf585554",
        "action_id": "29c46677c2abe690c"
      }
      """
    When I request "POST /hello/new-payment"
    Then the API response status code should be 204
