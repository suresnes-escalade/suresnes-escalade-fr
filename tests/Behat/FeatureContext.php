<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Tests\Behat;

use DateTime;
use Exception;
use Symfony\Component\Routing\RouterInterface;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends BaseContext
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct(private readonly Storage $storage, private readonly RouterInterface $router)
    {
    }

    /**
     * @Given I am on route :routeName
     * @Given I go to route :routeName
     */
    public function iAmOnRoute(string $routeName): void
    {
        $this->getSession()->visit($this->router->generate($routeName));
    }

    /**
     * @Given I am authenticated with username :username and password :password
     */
    public function iAmLoggedInAsWithPassword(string $username, string $password, bool $remember = false): void
    {
        $this->iAmOnRoute('app_login');
        $this->getMink()->getSession()->getPage()->fillField('Identifiant ou adresse e-mail', $username);
        $this->getMink()->getSession()->getPage()->fillField('Mot de passe', $password);
        if ($remember) {
            $this->getMink()->getSession()->getPage()->checkField('Se souvenir de moi');
        }
        $this->getMink()->getSession()->getPage()->pressButton('Se connecter');
    }

    /**
     * @Then I should not be allowed to see page
     */
    public function iShouldNotBeAllowedToSeePage(): void
    {
        $this->getMink()->assertSession()->statusCodeEquals(403);
    }

    /**
     * @Then I should see :name in slot :slot_title
     */
    public function iShouldSeeNameInSlot(string $name, string $slot_title): void
    {
        $text_to_match = $this->findTextForSlot($slot_title);

        if (!str_contains($text_to_match, $name)) {
            throw new Exception(sprintf('Subscription "%s" not found in the slot "%s", but should be', $name, $slot_title));
        }
    }

    /**
     * @Then I should not see :name in slot :slot_title
     */
    public function iShouldNotSeeNameInSlot(string $name, string $slot_title): void
    {
        $text_to_match = $this->findTextForSlot($slot_title);

        if (str_contains($text_to_match, $name)) {
            throw new Exception(sprintf('Subscription "%s" found in the slot "%s", but should not', $name, $slot_title));
        }
    }

    protected function findTextForSlot(string $slot_title): string
    {
        $slot = $this->getSession()->getPage()->find('css', sprintf('*[data-slot-id="%d"]', $this->storage->get('slot_' . $slot_title)));

        $text_to_match = str_replace(["\r", "\r\n", "\n", "\t"], '', $this->findByDataTestid('slot-subscription', $slot)->getText());

        return str_replace(['  '], ' ', $text_to_match);
    }

    /**
     * @Then I click :export_link for season :season_name
     */
    public function iClickForSeason(string $export_link, string $season_name): void
    {
        $seasonId = $this->storage->get('season_' . $season_name);

        $season = $this->getSession()->getPage()->find('css', sprintf('*[objectId="%d"]', $seasonId));
        if (!$season) {
            throw new Exception(sprintf('Season "%s" not found but should be', $season_name));
        }

        $season->getParent()->clickLink($export_link);
    }

    /**
     * @Then I should see in the header :header :value
     */
    public function iShouldSeeInTheHeader(string $header, string $value): void
    {
        $headerValue = $this->getSession()->getResponseHeader($header);
        if (!$headerValue || !str_contains($headerValue, $value)) {
            throw new Exception(sprintf('Did not see "%s" with value %s.', $header, $value));
        }
    }

    /**
     * @Then the number of lines in the csv file should be :number
     */
    public function theNumberOfLinesShouldBe(int $number): void
    {
        $values = str_getcsv($this->getSession()->getPage()->getContent(), ";");
        if (count($values) !== $number) {
            throw new Exception(sprintf('Found "%d" lines in csv file but expect "%d".', count($values), $number));
        }
    }

    /**
     * @Then the :field field should contain today
     */
    public function theFieldShouldContainToday(string $field): void
    {
        $this->assertSession()->fieldValueEquals($field, (new DateTime())->format('Y-m-d\TH:i'));
    }
}
