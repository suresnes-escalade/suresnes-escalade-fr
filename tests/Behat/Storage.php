<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Tests\Behat;

class Storage
{
    /**
     * @var array<string, int>
     */
    private array $data;

    public function set(string $name, int $value): void
    {
        $this->data[$name] = $value;
    }

    public function get(string $name): int
    {
        return $this->data[$name];
    }
}
