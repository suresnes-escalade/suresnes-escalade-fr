<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Tests\Behat;

use DateTime;
use Faker\Factory;
use App\Entity\Season;
use App\Entity\Slot;
use App\Entity\Subscription;
use App\Entity\User;
use App\Repository\SeasonRepository;
use App\Repository\SlotRepository;
use App\Repository\SubscriptionRepository;
use App\Repository\UserRepository;
use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\AfterScenarioScope;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Gherkin\Node\TableNode;
use Doctrine\ORM\EntityManagerInterface;

class DBContext implements Context
{
    public function __construct(
        private readonly Storage $storage,
        private readonly EntityManagerInterface $entityManager,
        private readonly SeasonRepository $seasonRepository,
        private readonly SlotRepository $slotRepository,
        private readonly SubscriptionRepository $subscriptionRepository,
        private readonly UserRepository $userRepository,
    ) {
    }

    /**
     * @Given a user:
     * @Given some users:
     */
    public function someUsers(TableNode $table): void
    {
        foreach ($table->getHash() as $userRow) {
            $user = new User();
            $user->setLogin($userRow['user_login']);
            $user->setPass(password_hash((string) $userRow['user_pass'], PASSWORD_DEFAULT));
            $user->setWordpressRoles([$userRow['role']]);
            $user->setRegistered(new DateTime());
            $this->userRepository->addUser($user);

            if ($user->getId() !== 0) {
                $this->storage->set('user_' . $userRow['user_login'], $user->getId());
            }
        }
    }

    /**
     * @Given a season:
     */
    public function aSeason(TableNode $table): void
    {
        foreach ($table->getHash() as $seasonRow) {
            $season = new Season();
            $season->setTitle($seasonRow['title']);
            $season->setBeginAt(new DateTime($seasonRow['beginAt']));
            $season->setEndAt(new DateTime($seasonRow['endAt']));

            // default values
            $season->setPriceLicenceChild(100);
            $season->setPriceLicenceAdult(200);
            $season->setPriceLicenceFamily(500);
            $season->setPriceFirstRegistration(120);
            $season->setPriceNotResidentChild(130);
            $season->setPriceNotResidentAdult(230);
            $season->setPriceCommittee(50);
            $season->setPriceClubChild(30);
            $season->setPriceClubAdult(50);
            $season->setPriceInsurance0(10);
            $season->setPriceInsurance1(10);
            $season->setPriceInsurance2(10);
            $season->setPriceInsurance3(10);
            $season->setPriceInsuranceSlackline(10);
            $season->setPriceInsuranceSki(10);
            $season->setPriceOptionIJ1(20);
            $season->setPriceOptionIJ2(20);
            $season->setPriceOptionIJ3(20);
            $this->seasonRepository->addSeason($season);

            if ($season->getId()) {
                $this->storage->set('season_' . $seasonRow['title'], $season->getId());
            }
        }
    }

    /**
     * @Given a slot:
     * @Given some slots:
     */
    public function someSlots(TableNode $table): void
    {
        foreach ($table->getHash() as $slotRow) {
            $season = $this->seasonRepository->findOneBy(['title' => $slotRow['season']]);

            $slot = new Slot();
            $slot->setTitle($slotRow['title']);
            $slot->setSeason($season);
            $slot->setDayOfWeek($slotRow['dayOfWeek']);
            $slot->setBeginTime(new DateTime($slotRow['beginTime']));
            $slot->setEndTime(new DateTime($slotRow['endTime']));
            $slot->setMaxPlaces($slotRow['maxPlaces']);
            $slot->setMarginPlaces($slotRow['marginPlaces']);
            $slot->setIsFull($slotRow['is_full']);
            $this->slotRepository->addSlot($slot);

            if ($slot->getId()) {
                $this->storage->set('slot_' . $slotRow['title'], $slot->getId());
            }
        }
    }

    /**
     * @Given a subscription:
     * @Given some subscriptions:
     */
    public function someSubscriptions(TableNode $table): void
    {
        $faker = Factory::create('fr_FR');

        foreach ($table->getHash() as $subscriptionRow) {
            $season = $this->seasonRepository->findOneBy(['title' => $subscriptionRow['season']]);
            $slot1 = $this->slotRepository->findOneBy(['title' => $subscriptionRow['slot1']]);
            $slot2 = $this->slotRepository->findOneBy(['title' => $subscriptionRow['slot2']]);

            $subscription = new Subscription();
            $subscription->setSeason($season);
            $subscription->setSlot1($slot1);
            $subscription->setSlot2($slot2);

            $subscription->setLastName($subscriptionRow['last_name']);
            $subscription->setFirstName($subscriptionRow['first_name']);
            $subscription->setBirthday(new DateTime($subscriptionRow['birthday']));
            $subscription->setBirthPlace($subscriptionRow['birth_place'] ?? $faker->city);

            // champs optionnels
            $subscription->setSex($subscriptionRow['sex'] ?? 'M');
            $subscription->setAddress($subscriptionRow['address'] ?? $faker->streetAddress);
            $subscription->setPostalCode($subscriptionRow['postalCode'] ?? $faker->postcode);
            $subscription->setCity($subscriptionRow['city'] ?? $faker->city);
            $subscription->setPhone($subscriptionRow['phone'] ?? '0123456789'); // need a faker provider
            $subscription->setEmail($subscriptionRow['email'] ?? $faker->email);
            $subscription->setCertificateDoctor($subscriptionRow['certificateDoctor'] ?? $faker->name);

            if (isset($subscriptionRow['status'])) {
                $subscription->setStatus((int) $subscriptionRow['status']);
            }

            $this->subscriptionRepository->addOrUpdate($subscription);
            if ($subscription->getId()) {
                $this->storage->set('subscription_' . $subscriptionRow['last_name'] . $subscriptionRow['first_name'], $subscription->getId());
            }
        }
    }

    /**
     * @BeforeScenario
     */
    public function prepareDB(BeforeScenarioScope $scope): void
    {
        $this->clearData();
    }

    /**
     * @AfterScenario
     */
    public function cleanDB(AfterScenarioScope $scope): void
    {
        $this->clearData();
    }

    protected function clearData(): void
    {
        $this->entityManager->createQuery('DELETE FROM App:Subscription')->execute();
        $this->entityManager->createQuery('DELETE FROM App:Slot')->execute();
        $this->entityManager->createQuery('DELETE FROM App:Season')->execute();
        $this->entityManager->createQuery('DELETE FROM App:UserMeta')->execute();
        $this->entityManager->createQuery('DELETE FROM App:User')->execute();
        $this->entityManager->createQuery('DELETE FROM App:Payment')->execute();
    }
}
