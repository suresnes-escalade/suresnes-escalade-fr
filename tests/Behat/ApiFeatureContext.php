<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Tests\Behat;

use Exception;
use Behat\Gherkin\Node\PyStringNode;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\HttpFoundation\Response;

class ApiFeatureContext extends BaseContext
{
    private string $requestPayload;

    public function __construct(private readonly KernelBrowser $client)
    {
    }

    /**
     * @Given I have the payload:
     */
    public function iHaveThePayload(PyStringNode $requestPayload): void
    {
        $this->requestPayload = (string) $requestPayload;
    }

    protected function getTestResponse(): Response
    {
        return $this->client->getResponse();
    }

    /**
     * @When /^I request "(GET|PUT|POST|DELETE|PATCH) ([^"]*)"$/
     */
    public function iRequest(string $httpMethod, string $uri): void
    {
        $files = [];
        $parameters = [];
        $headers = ['content-Type' => 'application/json'];

        $this->client->request($httpMethod, $uri, $parameters, $files, $headers, $this->requestPayload);
    }

    /**
     * @Then /^the API response status code should be (?P<code>\d+)$/
     */
    public function theResponseStatusCodeShouldBe(int $statusCode): void
    {
        if ($statusCode !== $this->getTestResponse()->getStatusCode()) {
            throw new Exception(sprintf('Expected status code "%s" does not match observed status code "%s"', $statusCode, $this->getTestResponse()->getStatusCode()));
        }
    }
}
