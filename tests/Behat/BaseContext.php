<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Tests\Behat;

use Exception;
use Behat\Behat\Context\Context;
use Behat\Mink\Element\NodeElement;
use Behat\MinkExtension\Context\RawMinkContext;
use Behat\Mink\Exception\ExpectationException;

abstract class BaseContext extends RawMinkContext implements Context
{
    public function findByDataTestid(string $data_id, NodeElement $parent = null): NodeElement
    {
        if (!$parent instanceof NodeElement) {
            $parent = $this->getSession()->getPage();
        }
        $element = $parent->find('css', sprintf('*[data-testid="%s"]', $data_id));

        if ($element === null) {
            throw new Exception(sprintf('Element with data-testid="%s" not found', $data_id));
        }

        return $element;
    }

    protected function throwExpectationException(string $message): void
    {
        throw new ExpectationException($message, $this->getSession());
    }
}
