<?php
/*
 * This file is part of suresnes-escalade website
 */

use Symfony\Component\Dotenv\Dotenv;

putenv('APP_ENV=' . $_SERVER['APP_ENV'] = $_ENV['APP_ENV'] = 'test');

if (method_exists(Dotenv::class, 'loadEnv')) {
    (new Dotenv())->loadEnv(dirname(__DIR__) . '/.env');
}
