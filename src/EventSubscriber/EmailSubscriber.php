<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\EventSubscriber;

use App\Entity\Subscription;
use App\Event\AppEvents;
use App\Event\SubscriptionEvent;
use Exception;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class EmailSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly MailerInterface $mailer)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            AppEvents::SUBSCRIPTION_SUBMITTED => 'onSubscriptionSubmitted',
            AppEvents::SUBSCRIPTION_VALIDATED => 'onSubscriptionValidated'
        ];
    }

    public function onSubscriptionSubmitted(SubscriptionEvent $event): void
    {
        $subscription = $event->getSubscription();

        $message = $this->createMessage($subscription)
            ->subject('Suresnes Escalade - Inscription en attente de validation ' . $subscription)
            ->textTemplate('email/text/submitted.text.twig')
            ->htmlTemplate('email/html/submitted.html.twig')
            ->context(['subscription' => $subscription]);

        $this->mailer->send($message);
    }

    public function onSubscriptionValidated(SubscriptionEvent $event): void
    {
        $subscription = $event->getSubscription();

        $message = $this->createMessage($subscription)
            ->subject('Suresnes Escalade - Validation de votre inscription ' . $subscription)
            ->textTemplate('email/text/validated.text.twig')
            ->htmlTemplate('email/html/validated.html.twig')
            ->context(['subscription' => $subscription]);

        $this->mailer->send($message);
    }

    private function createMessage(Subscription $subscription): TemplatedEmail
    {
        if (!$subscription->getEmail()) {
            throw new Exception('Email cannot be null');
        }

        $message = (new TemplatedEmail())
            ->from('inscription@suresnes-escalade.fr')
            ->to(new Address($subscription->getEmail(), $subscription->getLastName() . ' ' . $subscription->getFirstName()))
            ->bcc('inscription@suresnes-escalade.fr');

        if ($subscription->getParentEmail()) {
            $message->cc($subscription->getParentEmail());
        }

        return $message;
    }
}
