<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\EventSubscriber;

use DateTimeInterface;
use App\Event\AppEvents;
use DateTime;
use App\Event\SubscriptionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ActivationSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly TokenStorageInterface $tokenStorage)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            AppEvents::SUBSCRIPTION_UPDATED => 'onSubscriptionValidated'
        ];
    }

    public function onSubscriptionValidated(SubscriptionEvent $event): void
    {
        $subscription = $event->getSubscription();

        if (!$subscription->getActivatedAt() instanceof DateTimeInterface) {
            $subscription->setActivatedAt(new DateTime());
        }

        if ($subscription->getActivatedBy() === null && ($token = $this->tokenStorage->getToken()) && ($user = $token->getUser())) {
            $subscription->setActivatedBy($user->getUserIdentifier());
        }
    }
}
