<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Admin;

use App\Entity\Season;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollectionInterface;
use Sonata\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

/**
 * @extends AbstractAdmin<Season>
 */
class SeasonAdmin extends AbstractAdmin
{
    protected function generateBaseRouteName(bool $isChildAdmin = false): string
    {
        return 'admin_season';
    }

    protected function generateBaseRoutePattern(bool $isChildAdmin = false): string
    {
        return '/season';
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->addIdentifier('title', null, ['route' => ['name' => 'edit']])
            ->add('beginAt', null, ['format' => 'd/m/Y'])
            ->add('endAt', null, ['format' => 'd/m/Y'])
            ->add('_action', 'actions', [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                    'clone' => [
                        'template' => 'season_admin/list__action_clone.html.twig',
                    ],
                    'exportFFME' => [
                        'template' => 'season_admin/list__action_exportFFME.html.twig',
                    ],
                    'importFFME' => [
                        'template' => 'season_admin/list__action_importFFME.html.twig',
                    ],
                ]
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->tab('General')
            ->with('General')
            ->add('title')
            ->add('beginAt', DateType::class, ['widget' => 'single_text'])
            ->add('endAt', DateType::class, ['widget' => 'single_text'])
            ->end()
            ->end()
            ->tab('Tarif cotisation')
            ->with('Tarif cotisation')
            ->add('priceFirstRegistration')
            ->add('priceNotResidentChild')
            ->add('priceNotResidentAdult')
            ->add('priceCommittee')
            ->add('priceClubChild')
            ->add('priceClubAdult')
            ->end()
            ->end()
            ->tab('Tarif Licence')
            ->with('Tarif Licence')
            ->add('priceLicenceChild')
            ->add('priceLicenceAdult')
            ->add('priceLicenceFamily')
            ->end()
            ->end()
            ->tab('Tarif assurance')
            ->with('Tarif assurance')
            ->add('priceInsurance0')
            ->add('priceInsurance1')
            ->add('priceInsurance2')
            ->add('priceInsurance3')
            ->add('priceInsuranceSlackline')
            ->add('priceInsuranceSki')
            ->add('priceOptionIJ1')
            ->add('priceOptionIJ2')
            ->add('priceOptionIJ3')
            ->end()
            ->end()
            ->tab('Créneaux')
            ->with('Créneaux')
            ->add('slots', CollectionType::class, [
                'by_reference' => false,
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'position',
            ])
            ->end()
            ->end();
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('title')
            ->add('beginAt')
            ->add('endAt')
            ->add('slots');
    }

    protected function configureRoutes(RouteCollectionInterface $collection): void
    {
        $collection->add('clone', $this->getRouterIdParameter() . '/clone');
        $collection->add('exportFFME', $this->getRouterIdParameter() . '/exportFFME');
        $collection->add('importFFME', $this->getRouterIdParameter() . '/importFFME');
    }
}
