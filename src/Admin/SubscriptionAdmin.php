<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Admin;

use App\Entity\Subscription;
use App\Repository\SlotRepository;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollectionInterface;
use Sonata\DoctrineORMAdminBundle\Filter\ChoiceFilter;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * @extends AbstractAdmin<Subscription>
 */
class SubscriptionAdmin extends AbstractAdmin
{
    public function __construct(private readonly SlotRepository $slotRepository)
    {
    }

    protected function generateBaseRouteName(bool $isChildAdmin = false): string
    {
        return 'admin_subscription';
    }

    protected function generateBaseRoutePattern(bool $isChildAdmin = false): string
    {
        return '/subscription';
    }

    protected function configureRoutes(RouteCollectionInterface $collection): void
    {
        $collection->add('print', $this->getRouterIdParameter() . '/print');
        $collection->add('voucher', $this->getRouterIdParameter() . '/voucher');

        $collection->remove('create');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('season')
            ->add('lastName')
            ->add('birthName')
            ->add('firstName')
            ->add('status', ChoiceFilter::class, ['field_type' => ChoiceType::class, 'field_options' => ['choices' => array_flip(Subscription::getStatusChoices())]])
            ->add('certificateStatus', ChoiceFilter::class, ['field_type' => ChoiceType::class, 'field_options' => ['choices' => array_flip(Subscription::getCertificateStatusChoices())]])
            ->add('certificateDate')
            ->add('certificateOld')
            ->add('activatedBy')
            ->add('licence')
            ->add('slot1')
            ->add('slot2')
            ->add('activatedSlot');
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('season', null, ['header_class' => 'subscription-season'])
            ->addIdentifier('lastName', null, ['route' => ['name' => 'edit']])
            ->add('birthName')
            ->add('firstName')
            ->add('id')
            ->add('status', 'choice', ['choices' => Subscription::getStatusChoices()])
            ->add('createdAt', null, ['format' => 'd/m/Y'])
            ->add('age')
            ->add('priceCalculated')
            ->add('activatedSlot')
            ->add('licence')
            ->add('certificateStatus', 'choice', ['choices' => Subscription::getCertificateStatusChoices()])
            ->add('_action', 'actions', [
                'header_class' => 'subscription-actions',
                'actions' => [
                    'edit' => [],
                    'print' => [
                        'icon' => 'bidi.png',
                        'template' => 'admin/list__action_print.html.twig'
                    ],
                    'voucher' => [
                        'template' => 'admin/list__action_voucher.html.twig'
                    ],
                ]
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $subscription = $this->getSubject();
        $season = null;
        $query_slots = null;
        $certificatFileHelp = 'Aucun fichier';

        // subcription est null si on essaie de supprimer plusieurs adhésions.
        if ($subscription != null) {
            $season = $subscription->getSeason();
            if ($subscription->getCertificateFile()) {
                $certificatFileHelp = sprintf(
                    '<a href="%s" title="Télécharger" target="_blank">Afficher le fichier "%s"</a>',
                    $this->getRouteGenerator()->generate('simple_subscription_certificat', ['id' => $subscription->getId()]),
                    $subscription->getCertificateFile()
                );
            }

            if ($season !== null) {
                $query_slots = $this->slotRepository->getSlotsQueryForSeason($season);
            }
        }

        $formMapper
            ->tab('Profil du grimpeur')
            ->with('')
            ->add('id', null, ['disabled' => true])
            ->add('season', null, ['disabled' => true])
            ->add('lastName', null, ['required' => true])
            ->add('birthName', null, ['required' => false])
            ->add('firstName', null, ['required' => true])
            ->add('birthday', BirthdayType::class, ['widget' => 'single_text'])
            ->add('birthPlace', null, ['required' => true])
            ->add('age', null, ['disabled' => true])
            ->add('sex', ChoiceType::class, ['choices' => ['Masculin' => 'M', 'Féminin' => 'F'], 'expanded' => true])
            ->add('status', ChoiceType::class, ['required' => true, 'choices' => array_flip(Subscription::getStatusChoices())])
            ->add('activatedAt', null, ['disabled' => true, 'widget' => 'single_text'])
            ->add('activatedBy', null, ['disabled' => true])
            ->add('comment', TextareaType::class, ['required' => false])
            ->add('address')
            ->add('postalCode', TextType::class)
            ->add('city')
            ->add('phone', TextType::class)
            ->add('email', EmailType::class)
            ->add('occupation')
            ->add('nationality', ChoiceType::class, ['required' => true, 'choices' => array_flip(Subscription::getNationalityChoices())])
            ->end()
            ->end()
            ->tab('Adhérent parent')
            ->with('')
            ->add('parentLastName', null, ['required' => false])
            ->add('parentFirstName', null, ['required' => false])
            ->add('parentPhone', TextType::class, ['required' => false])
            ->add('parentEmail', EmailType::class, ['required' => false])
            ->add('goesAlone', CheckboxType::class, ['required' => false])
            ->end()
            ->end();

        if ($season !== null) {
            $formMapper
                ->tab('Créneau')
                ->with('')
                ->add('slot1', ModelType::class, ['property' => 'title', 'query' => $query_slots, 'btn_add' => false])
                ->add('slot2', ModelType::class, ['property' => 'title', 'query' => $query_slots, 'btn_add' => false])
                ->add('activatedSlot', ModelType::class, ['property' => 'title', 'required' => false, 'query' => $query_slots, 'btn_add' => false])
                ->add('noSlot', CheckboxType::class, ['required' => false])
                ->end()
                ->end();
        }

        $formMapper
            ->tab('Informations')
            ->with('')
            ->add('isRenewal', CheckboxType::class, ['required' => false])
            ->add('climbingLevel', ChoiceType::class, ['choices' => array_flip(Subscription::getClimbingLevels())])
            ->add('climbingExperience', ChoiceType::class, ['choices' => array_flip(Subscription::getClimbingExperiences())])
            ->add('passportFfme', ChoiceType::class, ['choices' => array_flip(Subscription::getPassportsFFME())])
            ->add('isCompetitor', ChoiceType::class, ['choices' => array_flip(Subscription::getIsCompetitorChoices())])
            ->end()
            ->end();

        if ($season !== null) {
            $formMapper
                ->tab('Licence & Assurance')
                ->with('')
                ->add('licence', TextType::class, ['required' => false])
                ->add('licenceDate', DateType::class, ['required' => false, 'widget' => 'single_text'])
                ->add('licenceFamily', CheckboxType::class, ['required' => false])
                ->add('insurance', ChoiceType::class, ['choices' => array_flip($season->getInsuranceChoices()), 'expanded' => true])
                ->add('insuranceIj', ChoiceType::class, ['choices' => array_flip($season->getInsuranceIjChoices()), 'expanded' => true])
                ->add('insuranceSlackline', ChoiceType::class, ['choices' => array_flip($season->getInsuranceSlacklineChoices()), 'expanded' => true])
                ->add('insuranceSki', ChoiceType::class, ['choices' => array_flip($season->getInsuranceSkiChoices()), 'expanded' => true])
                ->end()
                ->end();
        }
        $formMapper
            ->tab('Certificat médical')
            ->with('')
            ->add('certificateFile', FileType::class, ['required' => false, 'data_class' => null, 'help' => $certificatFileHelp])
            ->add('certificateDoctor', null, ['required' => false])
            ->add('certificateDate', DateType::class, ['required' => false, 'widget' => 'single_text'])
            ->add('certificateOld', ChoiceType::class, ['choices' => array_flip(Subscription::getCertificateOldChoices())])
            ->add('certificateCompetition', CheckboxType::class, ['required' => false])
            ->add('certificateStatus', ChoiceType::class, ['choices' => array_flip(Subscription::getCertificateStatusChoices())])
            ->end()
            ->end()
            ->tab('Comptabilité')
            ->with('')
                    // Payment
            ->add('payment3Times', CheckboxType::class, ['required' => false])
            ->add('paymentPass92', CheckboxType::class, ['required' => false])
            ->add('paymentCouponSport', CheckboxType::class, ['required' => false])
            ->add('paymentSubvensionCe', CheckboxType::class, ['required' => false])
            ->add('priceCalculated', null, ['required' => false, 'disabled' => true, 'help' => 'Recalculé à chaque enregistrement'])
            ->add('priceCorrected', null, ['required' => false])
            ->add('paidCash', null, ['required' => false])
            ->add('paidCB', null, ['required' => false])
            ->add('paidPass92', null, ['required' => false])
            ->add('paidCouponSport', null, ['required' => false])
            ->add('paidSubvensionCe', null, ['required' => false])
            ->add('paidRegul', null, ['required' => false, 'help' => 'Si cet adhérent a payé pour un autre adhérent, mettre le montant de la cotisation de l\'autre adhérent (sans signe négatif). Il faut également penser à mettre le montant négatif de la cotisation sur la fiche de l\'autre adhérent.'])
            ->add('paidCheck01', null, ['required' => false])
            ->add('paidCheck02', null, ['required' => false])
            ->add('paidCheck03', null, ['required' => false])
            ->add('paidCheckGarantee', null, ['required' => false])
            ->add('paidTotal', null, ['required' => false, 'disabled' => true])
            ->end()
            ->end();
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('userId')
            ->add('season')
            ->add('lastName')
            ->add('firstName')
            ->add('sex')
            ->add('status')
            ->add('createdAt')
            ->add('activatedAt')
            ->add('activatedBy')
            ->add('birthday')
            ->add('age')
            ->add('occupation')
            ->add('licence')
            ->add('licenceDate')
            ->add('slot1')
            ->add('slot2')
            ->add('activatedSlot')
            ->add('certificateFile')
            ->add('certificateDoctor')
            ->add('certificateDate');
    }

    public function getExportFormats(): array
    {
        return [
            'csv', 'xls'
        ];
    }

    public function configureExportFields(): array
    {
        $fields = [
            'season.title' => 'Saison',
            'id' => 'N°',
            'lastName' => 'Nom',
            'birthName' => 'Nom de naissance',
            'firstName' => 'Prénom',
            'birthday' => 'Date de naissance',
            'birthPlace' => 'Lieu de naissance',
            'age' => 'Age',
            'sex' => 'Sexe',
            'status' => 'Statut de l\'inscription',
            'activatedBy' => 'Activé par',
            'comment' => 'Commentaire',
            'address' => 'Adresse',
            'postalCode' => 'Code postal',
            'city' => 'Ville',
            'phone' => 'Téléphone',
            'email' => 'Email',
            // 'occupation' => 'Profession',
            'nationality' => 'Nationalité',
            'parentLastName' => 'Nom du parent',
            'parentFirstName' => 'Prénom du parent',
            'parentPhone' => 'Téléphone du parent',
            'parentEmail' => 'Email du parent',
            'goesAlone' => 'Rentre seul',
            //'slot1.title' => 'Créneau 1',
            //'slot2.title' => 'Créneau 2',
            'activatedSlot.title' => 'Créneau validé',
            'noSlot' => 'Aucun créneau',
            'isRenewal' => 'Renouvellement',
            //'isCompetitor' => 'Compétiteur',
            'licence' => 'Numéro de licence',
            'licenceDate' => 'Date de licence',
            'insurance' => 'Assurance indiv.',
            'insuranceIj' => 'Indemnités j.',
            'insuranceSlackline' => 'Slackline',
            'insuranceSki' => 'Assurance ski',
            // 'certificateFile' => 'certificateFile',
            'certificateOld' => 'certificateOld',
            'certificateStatus' => 'certificateStatus',
            //'certificateDoctor' => 'certificateDoctor',
            'certificateDate' => 'certificateDate',
            // 'certificateCompetition' => 'certificateCompetition',
            'payment3Times' => 'Paiement en 3 fois',
            'paymentPass92' => 'Pass 92',
            'paymentCouponSport' => 'Coupon Sport',
            'paymentSubvensionCe' => 'Subvension CE',
            'priceCalculated' => 'Prix calculé',
            'priceCorrected' => 'Prix corrigé',
            //'paidCash' => 'Payé cash',
            'paidCB' => 'Payé CB',
            'paidPass92' => 'Payé Pass 92',
            'paidCouponSport' => 'Payé Coupon Sport',
            'paidSubvensionCe' => 'Payé Subvension CE',
            'paidRegul' => 'Payé par régulation',
            'paidCheck01' => 'Payé chèque 1',
            //'paidCheck02' => 'Payé chèque 2',
            //'paidCheck03' => 'Payé chèque 3',
            //'paidCheckGarantee' => 'Payé chèque garantie',
            'paidTotal' => 'Payé total',
        ];

        return array_flip($fields);
    }
}
