<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Admin;

use Iterator;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\AdminBundle\Exporter\DataSourceInterface;
use Sonata\DoctrineORMAdminBundle\Exporter\DataSource;
use Sonata\Exporter\Source\DoctrineORMQuerySourceIterator;
use Symfony\Component\DependencyInjection\Attribute\AsDecorator;

#[AsDecorator('sonata.admin.data_source.orm')]
class DecoratingDataSource implements DataSourceInterface
{
    public function __construct(private DataSource $dataSource)
    {
    }

    public function createIterator(ProxyQueryInterface $query, array $fields): Iterator
    {
        /** @var DoctrineORMQuerySourceIterator $iterator */
        $iterator = $this->dataSource->createIterator($query, $fields);
        $iterator->setDateTimeFormat('d/m/Y');

        return $iterator;
    }
}
