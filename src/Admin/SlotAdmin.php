<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Admin;

use App\Entity\Slot;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;

/**
 * @extends AbstractAdmin<Slot>
 */
class SlotAdmin extends AbstractAdmin
{
    protected function generateBaseRouteName(bool $isChildAdmin = false): string
    {
        return 'admin_slot';
    }

    protected function generateBaseRoutePattern(bool $isChildAdmin = false): string
    {
        return '/slot';
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('season')
            ->add('title')
            ->add('isFull');
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('season')
            ->addIdentifier('title', null, ['route' => ['name' => 'edit']])
            ->add('description')
            ->add('dayOfWeek', 'choice', ['choices' => Slot::getDayChoices()])
            ->add('beginTime')
            ->add('endTime')
            ->add('maxPlaces')
            ->add('isFull', null, ['editable' => true])
            ->add('_action', 'actions', [
                'header_class' => 'slot-actions',
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ]
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('season')
            ->add('title')
            ->add('description', TextareaType::class, ['required' => false])
            ->add('dayOfWeek', ChoiceType::class, ['choices' => array_flip(Slot::getDayChoices())])
            ->add('beginTime', TimeType::class)
            ->add('endTime', TimeType::class)
            ->add('maxPlaces', IntegerType::class)
            ->add('marginPlaces', IntegerType::class)
            ->add('isFull', null, ['required' => false]);
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('title')
            ->add('description')
            ->add('dayOfWeek')
            ->add('beginTime')
            ->add('endTime')
            ->add('minBirth')
            ->add('maxBirth')
            ->add('isFull');
    }
}
