<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Admin;

use App\Entity\User;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

/**
 * @extends AbstractAdmin<User>
 */
class UserAdmin extends AbstractAdmin
{
    protected function generateBaseRouteName(bool $isChildAdmin = false): string
    {
        return 'admin_user';
    }

    protected function generateBaseRoutePattern(bool $isChildAdmin = false): string
    {
        return '/user';
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->addIdentifier('login', null, ['route' => ['name' => 'edit']])
            ->add('nicename')
            ->add('email')
            ->add('displayName')
            ->add('status')
            ->add('registered');
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('login')
            ->add('nicename')
            ->add('displayName')
            ->add('email')
            ->add('url')
            ->add('registered', DateTimeType::class, ['widget' => 'single_text']);
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('login')
            ->add('nicename')
            ->add('displayName')
            ->add('email')
            ->add('url')
            ->add('registered');
    }
}
