<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Admin;

use App\Entity\Payment;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollectionInterface;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\DateType;

/**
 * @extends AbstractAdmin<Payment>
 */
class PaymentAdmin extends AbstractAdmin
{
    protected function generateBaseRouteName(bool $isChildAdmin = false): string
    {
        return 'admin_payment';
    }

    protected function generateBaseRoutePattern(bool $isChildAdmin = false): string
    {
        return '/payment';
    }

    protected function configureRoutes(RouteCollectionInterface $collection): void
    {
        $collection->remove('create');
        $collection->remove('edit');
        $collection->remove('delete');
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->addIdentifier('payment_id', null, ['route' => ['name' => 'edit']])
            ->add('date')
            ->add('payer_first_name')
            ->add('payer_last_name')
            ->add('amout')
            ->add('url')
            ->add('type')
            ->add('url_receipt')
            ->add('url_tax_receipt')
            ->add('action_id');
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('payment_id')
            ->add('date', DateType::class, ['widget' => 'single_text'])
            ->add('payer_first_name')
            ->add('payer_last_name')
            ->add('amout')
            ->add('url')
            ->add('type')
            ->add('url_receipt')
            ->add('url_tax_receipt')
            ->add('action_id');
    }
}
