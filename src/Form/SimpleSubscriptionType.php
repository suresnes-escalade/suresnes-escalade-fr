<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Form;

use App\Entity\Season;
use App\Entity\Subscription;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SimpleSubscriptionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Season $season */
        $season = $options['currentSeason'];

        $builder
            ->add('lastName', TextType::class, ['attr' => ['placeholder' => 'Nom']])
            ->add('birthName', TextType::class, ['required' => false,
                'attr' => ['placeholder' => 'Nom de naissance']])
            ->add('firstName', TextType::class, ['attr' => ['placeholder' => 'Prénom']])
            ->add('birthday', BirthdayType::class, ['placeholder' => '--', 'widget' => 'single_text'])
            ->add('birthPlace', TextType::class, ['attr' => ['placeholder' => 'Lieu de naissance']])
            ->add('sex', ChoiceType::class, ['choices' => ['Masculin' => 'M', 'Féminin' => 'F'],
                'placeholder' => 'Précisez :'])
            ->add('nationality', ChoiceType::class, ['choices' => array_flip(Subscription::getNationalityChoices())])
            // ->add('nationality', CountryType::class)
            ->add('address', TextType::class, ['attr' => ['placeholder' => 'Adresse']])
            ->add('postalCode', TextType::class, ['attr' => ['placeholder' => 'Code postal']])
            ->add('city', TextType::class, ['attr' => ['placeholder' => 'Ville']])
            ->add('phone', TextType::class, ['attr' => ['placeholder' => 'Téléphone']])
            ->add('email', EmailType::class, ['attr' => ['placeholder' => 'Email']])
            ->add('isRenewal', ChoiceType::class, ['choices' => ['Non' => 0, 'Oui' => 1],
                'placeholder' => 'Indiquez :']);
        //    ->add('isRenewal', ChoiceType::class, ['choices' => ['Non' => 0, 'Oui' => 1]]);

        $builder
            ->add('parentLastName', TextType::class, ['required' => false, 'attr' => ['placeholder' => 'Nom parent']])
            ->add('parentFirstName', TextType::class, ['required' => false, 'attr' => ['placeholder' => 'Prénom parent']])
            ->add('parentPhone', TextType::class, ['required' => false, 'attr' => ['placeholder' => 'Téléphone parent']])
            ->add('parentEmail', EmailType::class, ['required' => false, 'attr' => ['placeholder' => 'Email parent']])
            ->add('goesAlone', ChoiceType::class, ['required' => false, 'choices' => ['Non' => 0, 'Oui' => 1]]);

        $options = [
            'choices' => $season->getSlots()->filter(fn ($slot) => !$slot->getIsFull()),
            'choice_label' => 'label',
            'required' => true,
            'placeholder' => '-',
        ];

        $builder
            ->add('slot1', null, $options)
            ->add('slot2', null, $options);
        // ->add('noSlot', CheckboxType::class, ['required' => false]);

        // $builder->add('climbingLevel', ChoiceType::class, ['choices' => array_flip(Subscription::getClimbingLevels())]);

        $builder
            ->add('licence', TextType::class, ['required' => false, 'attr' => ['placeholder' => 'N° de licence']])
            ->add('insurance', ChoiceType::class, ['choices' => array_flip($season->getInsuranceChoicesForSubscription())])
            ->add('insuranceIj', ChoiceType::class, ['choices' => array_flip($season->getInsuranceIjChoices())])
            ->add('insuranceSlackline', ChoiceType::class, ['choices' => array_flip($season->getInsuranceSlacklineChoices())])
            ->add('insuranceSki', ChoiceType::class, ['choices' => array_flip($season->getInsuranceSkiChoices())]);

        $builder
        //    ->add('certificateOld', ChoiceType::class, ['choices' => array_flip(Subscription::getCertificateOldChoices()), 'placeholder' => 'Précisez :']);
            ->add('certificateOld', ChoiceType::class, ['placeholder' => 'Précisez :',
                'choices' => array_flip(Subscription::getCertificateOldChoices($season->getBeginAt()->format('Y')))]);
        //    ->add('certificateDoctor', TextType::class, ['required' => false])
        //    ->add('certificateCompetition', CheckboxType::class, ['required' => false])
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Subscription::class
        ]);
        $resolver->setRequired('currentSeason');
        $resolver->setAllowedTypes('currentSeason', Season::class);
    }

    public function getBlockPrefix(): string
    {
        return 'app_appbundle_subscription';
    }
}
