<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\EventListener;

use App\Entity\Subscription;
use App\Event\AppEvents;
use App\Event\SubscriptionEvent;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

#[AsEntityListener(event: Events::preUpdate, method: 'preUpdate', entity: Subscription::class)]
class SubscriptionListener
{
    public function __construct(private readonly EventDispatcherInterface $dispatcher)
    {
    }

    public function preUpdate(Subscription $subscription, PreUpdateEventArgs $preUpdateEventArgs): void
    {
        $event = new SubscriptionEvent($subscription);
        $this->dispatcher->dispatch($event, AppEvents::SUBSCRIPTION_UPDATED);

        $changeSet = $preUpdateEventArgs->getEntityChangeSet();

        if (isset($changeSet['status'])) {
            if ($subscription->isActive()) {
                $this->dispatcher->dispatch($event, AppEvents::SUBSCRIPTION_VALIDATED);
            }

            if ($subscription->isInactive()) {
                $this->dispatcher->dispatch($event, AppEvents::SUBSCRIPTION_CANCELLED);
            }
        }
    }
}
