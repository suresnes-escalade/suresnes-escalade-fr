<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Block;

use App\Entity\Subscription;
use App\Repository\SeasonRepository;
use App\Repository\SlotRepository;
use App\Repository\SubscriptionRepository;
use Sonata\AdminBundle\Admin\Pool;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Twig\Environment;

class StatsBlockService extends AbstractBlockService
{
    public function __construct(
        Environment $twig,
        private readonly Pool $pool,
        private readonly SeasonRepository $seasonRepository,
        private readonly SubscriptionRepository $subscriptionRepository,
        private readonly SlotRepository $slotRepository
    ) {
        parent::__construct($twig);
    }

    public function execute(BlockContextInterface $blockContext, ?Response $response = null): Response
    {
        /** @var string $code */
        $code = $blockContext->getSetting('code');
        $admin = $this->pool->getAdminByAdminCode($code);

        $current_season = $this->seasonRepository->findCurrent();
        $count = null;
        $all = null;

        if ($blockContext->getSetting('code') === 'app.admin.subscription') {
            $count = $this->subscriptionRepository->countForCurrentSeason(Subscription::STATUS_ACTIVE);
            $all = $this->subscriptionRepository->countForCurrentSeason();
        } elseif ($blockContext->getSetting('code') === 'app.admin.slot') {
            $count = $this->slotRepository->countForCurrentSeason();
            $all = null;
        }

        $blockContext->setSetting('filters', ['season' => ['value' => $current_season->getId()]]);

        return $this->renderResponse($blockContext->getTemplate(), [
            'block' => $blockContext->getBlock(),
            'settings' => $blockContext->getSettings(),
            'admin_pool' => $this->pool,
            'admin' => $admin,
            'count' => $count,
            'all' => $all,
        ], $response)
            ->setTtl(0)
            ->setPrivate();
    }

    public function getName(): string
    {
        return 'Subscriptions for current season';
    }

    public function configureSettings(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'icon' => 'fa-deviantart',
            'text' => 'Subscriptions',
            'translation_domain' => null,
            'color' => 'bg-yellow',
            'code' => false,
            'filters' => [],
            'limit' => 1000,
            'template' => 'admin/block_subscriptions.html.twig',
        ]);
    }
}
