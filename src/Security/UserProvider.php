<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Security;

use App\Entity\User;
use App\Entity\UserMeta;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * @implements UserProviderInterface<UserInterface>
 */
class UserProvider implements UserProviderInterface
{
    public function __construct(private readonly UserRepository $userRepository)
    {
    }

    public function supportsClass(string $class): bool
    {
        return User::class === $class;
    }

    public function loadUserByIdentifier(string $identifier): UserInterface
    {
        return $this->fetchUser($identifier);
    }

    public function refreshUser(UserInterface $user): UserInterface
    {
        return $this->fetchUser($user->getUserIdentifier());
    }

    protected function fetchUser(string $identifier): UserInterface
    {
        $user = $this->userRepository->findOneByUsernameOrEmail($identifier);

        // pretend it returns a User on success, null if there is no user
        if (is_null($user)) {
            throw new UserNotFoundException(sprintf('User with username "%s" does not exist.', $identifier));
        }

        $roles = [];
        $metas = $user->getMetas()->filter(fn (UserMeta $meta) => $meta->getKey() === User::WP_META_KEY_ROLES);

        if (!$metas->isEmpty() && $metas->first()->getValue()) {
            $capabilities = unserialize($metas->first()->getValue());

            if (is_array($capabilities)) {
                foreach (array_keys($capabilities) as $role) {
                    $roles[] = 'ROLE_WP_' . strtoupper($role);
                }
            }
        }

        $user->setRoles($roles);

        return $user;
    }
}
