<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use App\Repository\UserMetaRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * UserMeta
 */
#[ORM\Entity(repositoryClass: UserMetaRepository::class)]
#[ORM\Table(name: 'wp_usermeta')]
#[ORM\Index(name: 'user_id', columns: ['user_id'])]
#[ORM\Index(name: 'meta_key', columns: ['meta_key'])]
class UserMeta
{
    #[ORM\Column(name: 'umeta_id', type: Types::INTEGER)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private int $id;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'metas')]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'ID')]
    private ?User $user = null;

    #[ORM\Column(name: 'meta_key', type: Types::STRING, length: 255, nullable: true)]
    private ?string $key = null;

    #[ORM\Column(name: 'meta_value', type: Types::TEXT, nullable: true)]
    private ?string $value = null;

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setKey(string $key): self
    {
        $this->key = $key;

        return $this;
    }

    public function getKey(): ?string
    {
        return $this->key;
    }

    public function setValue(mixed $value): self
    {
        if (is_array($value) || is_object($value)) {
            $this->value = serialize($value);
        } elseif (is_string($value)) {
            $this->value = $value;
        }

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }
}
