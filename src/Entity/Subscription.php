<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use App\Repository\SubscriptionRepository;
use DateTime;
use Stringable;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Subscription
 */
#[ORM\Entity(repositoryClass: SubscriptionRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[ORM\Table(name: 'wp_app_subscription')]
#[ORM\Index(name: 'ix_status', columns: ['status'])]
class Subscription implements Stringable
{
    public const STATUS_INACTIVE = -1;
    public const STATUS_WAITING = 0;
    public const STATUS_ACTIVE = 1;
    public const STATUS_PAYMENT_PENDING = 2;
    public const STATUS_WAITING_PLACE = 3;
    public const CERTIFICATE_ABSENT = 0;
    public const CERTIFICATE_WAITING = 1;
    public const CERTIFICATE_VALID = 2;

    #[ORM\Column(name: 'id', type: Types::INTEGER)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private int $id;

    #[ORM\Column(name: 'user_id', type: Types::INTEGER, nullable: true)]
    private ?int $userId = null;

    #[Assert\NotBlank]
    #[ORM\ManyToOne(targetEntity: Season::class, fetch: 'EAGER')]
    private ?Season $season = null;

    #[Assert\NotBlank]
    #[Assert\Regex(pattern: '/\d/', match: false, message: 'Votre nom de famille ne peut pas contenir de chiffre.')]
    #[ORM\Column(name: 'last_name', type: Types::STRING, length: 255, nullable: true)]
    private ?string $lastName = null;

    #[Assert\Regex(pattern: '/\d/', match: false, message: 'Votre nom de naissance ne peut pas contenir de chiffre.')]
    #[ORM\Column(name: 'birth_name', type: Types::STRING, length: 255, nullable: true)]
    private ?string $birthName = null;

    #[Assert\NotBlank]
    #[Assert\Regex(pattern: '/\d/', match: false, message: 'Votre prénom ne peut pas contenir de chiffre.')]
    #[ORM\Column(name: 'first_name', type: Types::STRING, length: 255, nullable: true)]
    private ?string $firstName = null;

    #[Assert\NotBlank(groups: ['parent'])]
    #[Assert\Regex(pattern: '/\d/', match: false, message: 'Votre nom ne peut pas contenir de chiffre.')]
    #[ORM\Column(name: 'parent_last_name', type: Types::STRING, length: 255, nullable: true)]
    private ?string $parentLastName = null;

    #[Assert\NotBlank(groups: ['parent'])]
    #[Assert\Regex(pattern: '/\d/', match: false, message: 'Votre prénom ne peut pas contenir de chiffre.')]
    #[ORM\Column(name: 'parent_first_name', type: Types::STRING, length: 255, nullable: true)]
    private ?string $parentFirstName = null;

    #[ORM\Column(name: 'birthday', type: Types::DATE_MUTABLE, nullable: true)]
    private ?DateTimeInterface $birthday = null;

    #[ORM\Column(name: 'birth_place', type: Types::STRING, length: 255, nullable: true)]
    private ?string $birthPlace = null;

    #[ORM\Column(name: 'age', type: Types::FLOAT, nullable: true)]
    private ?float $age = null;

    #[Assert\NotBlank]
    #[ORM\Column(name: 'sex', type: Types::STRING, length: 1, nullable: true)]
    private ?string $sex = null;

    #[Assert\NotBlank]
    #[ORM\Column(name: 'address', type: Types::STRING, length: 500, nullable: true)]
    private ?string $address = null;

    #[Assert\NotBlank]
    #[ORM\Column(name: 'postalCode', type: Types::STRING, length: 10, nullable: true)]
    private ?string $postalCode = null; // = '92150';

    #[Assert\NotBlank]
    #[ORM\Column(name: 'city', type: Types::STRING, length: 255, nullable: true)]
    private ?string $city = null; // = 'Suresnes';

    #[Assert\NotBlank]
    #[Assert\Regex(pattern: '~0\d{9}~', message: 'Le numéro de téléphone doit être au format 0123456789')]
    #[ORM\Column(name: 'phone', type: Types::STRING, length: 10, nullable: true)]
    private ?string $phone = null;

    #[Assert\Regex(pattern: '~0\d{9}~', message: 'Le numéro de téléphone doit être au format 0123456789')]
    #[ORM\Column(name: 'parent_phone', type: Types::STRING, length: 10, nullable: true)]
    private ?string $parentPhone = null;

    #[Assert\NotBlank]
    #[ORM\Column(name: 'email', type: Types::STRING, length: 255, nullable: true)]
    private ?string $email = null;

    #[ORM\Column(name: 'parent_email', type: Types::STRING, length: 255, nullable: true)]
    private ?string $parentEmail = null;

    #[ORM\Column(name: 'goes_alone', type: Types::BOOLEAN)]
    private bool $goesAlone = false;

    #[Assert\NotBlank]
    #[ORM\Column(name: 'climbing_level', type: Types::STRING, length: 20, nullable: true)]
    private ?string $climbingLevel = 'bon';

    #[Assert\NotBlank(message: "Il faut repondre pour l'experience")]
    #[ORM\Column(name: 'climbing_experience', type: Types::STRING, length: 20, nullable: true)]
    private ?string $climbingExperience = 'aucun';

    #[Assert\NotBlank(message: 'Il faut repondre pour le passeport')]
    #[ORM\Column(name: 'passport_ffme', type: Types::STRING, length: 20, nullable: true)]
    private ?string $passportFfme = 'aucun';

    #[ORM\Column(name: 'is_competitor', type: Types::INTEGER)]
    private int $isCompetitor = 0;

    #[ORM\Column(name: 'status', type: Types::INTEGER)]
    private int $status = self::STATUS_PAYMENT_PENDING;

    #[ORM\Column(name: 'is_renewal', type: Types::BOOLEAN, nullable: true)]
    private ?bool $isRenewal = false;

    #[ORM\Column(name: 'created_at', type: Types::DATETIME_MUTABLE)]
    private DateTimeInterface $createdAt;

    #[ORM\Column(name: 'activated_at', type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $activatedAt = null;

    #[ORM\Column(name: 'activated_by', type: Types::STRING, length: 255, nullable: true)]
    private ?string $activatedBy = null;

    #[ORM\Column(name: 'occupation', type: Types::STRING, length: 255, nullable: true)]
    private ?string $occupation = null;

    #[ORM\Column(name: 'nationality', type: Types::STRING, length: 2, nullable: true, options: ['default' => 'FR'])]
    private ?string $nationality = 'FR';

    #[Assert\Range(min: 0, max: 10000000)]
    #[ORM\Column(name: 'licence', type: Types::STRING, length: 10, nullable: true)]
    private ?string $licence = null;

    #[ORM\Column(name: 'licence_family', type: Types::BOOLEAN)]
    private bool $licenceFamily = false;

    #[ORM\Column(name: 'licence_date', type: Types::DATE_MUTABLE, nullable: true)]
    private ?DateTimeInterface $licenceDate = null;

    #[ORM\Column(name: 'no_slot', type: Types::BOOLEAN)]
    private bool $noSlot = false;

    #[ORM\ManyToOne(targetEntity: Slot::class, fetch: 'EAGER', inversedBy: 'waitingSubscriptions')]
    private ?Slot $slot1 = null;

    #[ORM\ManyToOne(targetEntity: Slot::class, fetch: 'EAGER')]
    private ?Slot $slot2 = null;

    #[ORM\ManyToOne(targetEntity: Slot::class, fetch: 'EAGER', inversedBy: 'subscriptions')]
    #[ORM\JoinColumn(name: 'activatedSlot_id')]
    private ?Slot $activatedSlot = null;

    #[Assert\File]
    #[ORM\Column(name: 'certificate_file', type: Types::STRING, length: 255, nullable: true)]
    private ?string $certificateFile = null;

    #[ORM\Column(name: 'certificate_doctor', type: Types::STRING, length: 255, nullable: true)]
    private ?string $certificateDoctor = null; // @Assert\NotBlank(groups={"certificate"})

    #[ORM\Column(name: 'certificate_date', type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $certificateDate = null;

    #[ORM\Column(name: 'certificate_old', type: Types::BOOLEAN, nullable: true)]
    private ?bool $certificateOld = false;

    #[ORM\Column(name: 'certificate_competition', type: Types::BOOLEAN)]
    private bool $certificateCompetition = false;

    #[ORM\Column(name: 'certification_status', type: Types::INTEGER)]
    private int $certificateStatus = 0;

    #[ORM\Column(name: 'insurance', type: Types::INTEGER)]
    private int $insurance = 1;

    #[ORM\Column(name: 'insurance_ij', type: Types::INTEGER)]
    private int $insuranceIj = 0;

    #[ORM\Column(name: 'insurance_slackline', type: Types::INTEGER)]
    private int $insuranceSlackline = 0;

    #[ORM\Column(name: 'insurance_ski', type: Types::INTEGER)]
    private int $insuranceSki = 0;

    #[ORM\Column(name: 'payment_3_times', type: Types::BOOLEAN)]
    private bool $payment3Times = false;

    #[ORM\Column(name: 'payment_pass92', type: Types::BOOLEAN)]
    private bool $paymentPass92 = false;

    #[ORM\Column(name: 'payment_coupon_sport', type: Types::BOOLEAN)]
    private bool $paymentCouponSport = false;

    #[ORM\Column(name: 'payment_subvension_ce', type: Types::BOOLEAN)]
    private bool $paymentSubvensionCe = false;

    #[ORM\Column(name: 'price_calculated', type: Types::FLOAT)]
    private float $priceCalculated = 0;

    #[ORM\Column(name: 'price_corrected', type: Types::FLOAT)]
    private float $priceCorrected = 0;

    #[ORM\Column(name: 'paid_total', type: Types::FLOAT)]
    private float $paidTotal = 0;

    #[ORM\Column(name: 'paid_cash', type: Types::FLOAT)]
    private float $paidCash = 0;

    #[ORM\Column(name: 'paid_cb', type: Types::FLOAT)]
    private float $paidCB = 0;

    #[ORM\Column(name: 'paid_pass92', type: Types::FLOAT)]
    private float $paidPass92 = 0;

    #[ORM\Column(name: 'paid_coupon_sport', type: Types::FLOAT)]
    private float $paidCouponSport = 0;

    #[ORM\Column(name: 'paid_subvension_ce', type: Types::FLOAT)]
    private float $paidSubvensionCe = 0;

    #[ORM\Column(name: 'paid_regul', type: Types::FLOAT)]
    private float $paidRegul = 0;

    #[ORM\Column(name: 'paid_check01', type: Types::FLOAT)]
    private float $paidCheck01 = 0;

    #[ORM\Column(name: 'paid_check02', type: Types::FLOAT)]
    private float $paidCheck02 = 0;

    #[ORM\Column(name: 'paid_check03', type: Types::FLOAT)]
    private float $paidCheck03 = 0;

    #[ORM\Column(name: 'paid_check_garantee', type: Types::FLOAT)]
    private float $paidCheckGarantee = 0;

    #[ORM\Column(name: 'secret_key', type: Types::STRING, length: 50, nullable: true)]
    private ?string $key;

    #[ORM\Column(name: 'comment', type: Types::TEXT, nullable: true)]
    private ?string $comment = null;

    public function __construct()
    {
        $this->createdAt = new DateTime();
        $this->key = md5(uniqid('se', false));
    }

    /**
     * @return array<string, string>
     */
    public static function getClimbingLevels(): array
    {
        return [
            'debutant' => 'Débutant (n\'a jamais pratiqué)',
            'debrouille' => 'Débrouillé (sait grimper en moulinette)',
            'moyen' => 'Moyen (sait grimper en tête et en moulinette)',
            'bon' => 'Bon (sait grimper en tête du 6a à vue)',
            'tresbon' => 'Très Bon (sait grimper en tête du 7a et plus à vue)',
            'BE' => 'B.E. (sait regarder les autres grimper)',
        ];
    }

    /**
     * @return array<string, string>
     */
    public static function getClimbingExperiences()
    {
        return [
            'aucun' => 'Aucune, je suis débutant',
            'unpeu' => 'Moins d\'une année',
            '1an' => 'J\'en ai déjà fait durant 1 an',
            '2ans' => 'J\'en ai déjà fait durant 2 ans',
            '3ans' => 'J\'en ai déjà fait durant 3 ans',
            '4ans' => 'J\'en ai déjà fait durant 4 ans',
            '5ans_et_plus' => 'J\'en ai déjà fait durant 5 ans ou plus',
        ];
    }

    /**
     * @return array<string, string>
     */
    public static function getPassportsFFME()
    {
        return [
            'aucun' => 'Je ne possède malheureusement aucun Passeport',
            'premier_pas' => 'Passeport "Premier Pas"',
            'blanc' => 'Passeport Blanc',
            'jaune' => 'Passeport Jaune',
            'orange' => 'Passeport Orange',
            'vert' => 'Passeport Vert',
            'bleu' => 'Passeport Bleu',
            'violet' => 'Passeport Violet',
            'rouge' => 'Passeport Rouge',
            'noir' => 'Passeport Noir',
        ];
    }

    /**
     * @return array<string, string>
     */
    public static function getNationalityChoices()
    {
        static $nationalities = [
            'AF' => 'Afghanistan',
            'ZA' => 'Afrique du Sud',
            'AL' => 'Albanie',
            'DZ' => 'Algérie',
            'DE' => 'Allemagne',
            'AD' => 'Andorre',
            'AO' => 'Angola',
            'AI' => 'Anguilla',
            'AQ' => 'Antarctique',
            'AG' => 'Antigua-et-Barbuda',
            'AN' => 'Antilles néerlandaises',
            'SA' => 'Arabie saoudite',
            'AR' => 'Argentine',
            'AM' => 'Arménie',
            'AW' => 'Aruba',
            'AU' => 'Australie',
            'AT' => 'Autriche',
            'AZ' => 'Azerbaïdjan',
            'BS' => 'Bahamas',
            'BH' => 'Bahreïn',
            'BD' => 'Bangladesh',
            'BB' => 'Barbade',
            'PW' => 'Belau',
            'BE' => 'Belgique',
            'BZ' => 'Belize',
            'BJ' => 'Bénin',
            'BM' => 'Bermudes',
            'BT' => 'Bhoutan',
            'BY' => 'Biélorussie',
            'MM' => 'Birmanie',
            'BO' => 'Bolivie',
            'BA' => 'Bosnie-Herzégovine',
            'BW' => 'Botswana',
            'BR' => 'Brésil',
            'BN' => 'Brunei',
            'BG' => 'Bulgarie',
            'BF' => 'Burkina Faso',
            'BI' => 'Burundi',
            'KH' => 'Cambodge',
            'CM' => 'Cameroun',
            'CA' => 'Canada',
            'CV' => 'Cap-Vert',
            'CL' => 'Chili',
            'CN' => 'Chine',
            'CY' => 'Chypre',
            'CO' => 'Colombie',
            'KM' => 'Comores',
            'CG' => 'Congo',
            'KP' => 'Corée du Nord',
            'KR' => 'Corée du Sud',
            'CR' => 'Costa Rica',
            'CI' => 'Côte d\'Ivoire',
            'HR' => 'Croatie',
            'CU' => 'Cuba',
            'DK' => 'Danemark',
            'DJ' => 'Djibouti',
            'DM' => 'Dominique',
            'EG' => 'Égypte',
            'AE' => 'Émirats arabes unis',
            'EC' => 'Équateur',
            'ER' => 'Érythrée',
            'ES' => 'Espagne',
            'EE' => 'Estonie',
            'US' => 'États-Unis',
            'ET' => 'Éthiopie',
            // 'MK' => 'ex-République yougoslave de Macédoine',
            'FI' => 'Finlande',
            'FR' => 'France',
            'GA' => 'Gabon',
            'GM' => 'Gambie',
            'GE' => 'Géorgie',
            'GH' => 'Ghana',
            'GI' => 'Gibraltar',
            'GR' => 'Grèce',
            'GD' => 'Grenade',
            'GL' => 'Groenland',
            'GP' => 'Guadeloupe',
            'GU' => 'Guam',
            'GT' => 'Guatemala',
            'GN' => 'Guinée',
            'GQ' => 'Guinée équatoriale',
            'GW' => 'Guinée-Bissao',
            'GY' => 'Guyana',
            'GF' => 'Guyane française',
            'HT' => 'Haïti',
            'HN' => 'Honduras',
            'HK' => 'Hong Kong',
            'HU' => 'Hongrie',
            'BV' => 'Ile Bouvet',
            'CX' => 'Ile Christmas',
            'NF' => 'Ile Norfolk',
            'KY' => 'Iles Cayman',
            'CK' => 'Iles Cook',
            'CC' => 'Iles des Cocos (Keeling)',
            'FK' => 'Iles Falkland',
            'FO' => 'Iles Féroé',
            'FJ' => 'Iles Fidji',
            'GS' => 'Iles Géorgie du Sud et Sandwich du Sud',
            'HM' => 'Iles Heard et McDonald',
            'MH' => 'Iles Marshall',
            'UM' => 'Iles mineures éloignées des États-Unis',
            'PN' => 'Iles Pitcairn',
            'SB' => 'Iles Salomon',
            'SJ' => 'Iles Svalbard et Jan Mayen',
            'TC' => 'Iles Turks-et-Caicos',
            'VI' => 'Iles Vierges américaines',
            'VG' => 'Iles Vierges britanniques',
            'IN' => 'Inde',
            'ID' => 'Indonésie',
            'IR' => 'Iran',
            'IQ' => 'Iraq',
            'IE' => 'Irlande',
            'IS' => 'Islande',
            'IL' => 'Israël',
            'IT' => 'Italie',
            'JM' => 'Jamaïque',
            'JP' => 'Japon',
            'JO' => 'Jordanie',
            'KZ' => 'Kazakhstan',
            'KE' => 'Kenya',
            'KG' => 'Kirghizistan',
            'KI' => 'Kiribati',
            'KW' => 'Koweït',
            'LA' => 'Laos',
            'LS' => 'Lesotho',
            'LV' => 'Lettonie',
            'LB' => 'Liban',
            'LR' => 'Liberia',
            'LY' => 'Libye',
            'LI' => 'Liechtenstein',
            'LT' => 'Lituanie',
            'LU' => 'Luxembourg',
            'MO' => 'Macao',
            'MK' => 'Macédoine',
            'MG' => 'Madagascar',
            'MY' => 'Malaisie',
            'MW' => 'Malawi',
            'MV' => 'Maldives',
            'ML' => 'Mali',
            'MT' => 'Malte',
            'MP' => 'Mariannes du Nord',
            'MA' => 'Maroc',
            'MQ' => 'Martinique',
            'MU' => 'Maurice',
            'MR' => 'Mauritanie',
            'YT' => 'Mayotte',
            'MX' => 'Mexique',
            'FM' => 'Micronésie',
            'MD' => 'Moldavie',
            'MC' => 'Monaco',
            'MN' => 'Mongolie',
            'MS' => 'Montserrat',
            'MZ' => 'Mozambique',
            'NA' => 'Namibie',
            'NR' => 'Nauru',
            'NP' => 'Népal',
            'NI' => 'Nicaragua',
            'NE' => 'Niger',
            'NG' => 'Nigeria',
            'NU' => 'Nioué',
            'NO' => 'Norvège',
            'NC' => 'Nouvelle-Calédonie',
            'NZ' => 'Nouvelle-Zélande',
            'OM' => 'Oman',
            'UG' => 'Ouganda',
            'UZ' => 'Ouzbékistan',
            'PK' => 'Pakistan',
            'PA' => 'Panama',
            'PG' => 'Papouasie-Nouvelle-Guinée',
            'PY' => 'Paraguay',
            'NL' => 'Pays-Bas',
            'PE' => 'Pérou',
            'PH' => 'Philippines',
            'PL' => 'Pologne',
            'PF' => 'Polynésie française',
            'PR' => 'Porto Rico',
            'PT' => 'Portugal',
            'QA' => 'Qatar',
            'CF' => 'République centrafricaine',
            'CD' => 'République démocratique du Congo',
            'DO' => 'République dominicaine',
            'CZ' => 'République tchèque',
            'RE' => 'Réunion',
            'RO' => 'Roumanie',
            'GB' => 'Royaume-Uni',
            'RU' => 'Russie',
            'RW' => 'Rwanda',
            'EH' => 'Sahara occidental',
            'KN' => 'Saint-Christophe-et-Niévès',
            'SM' => 'Saint-Marin',
            'PM' => 'Saint-Pierre-et-Miquelon',
            'VA' => 'Saint-Siège',
            'VC' => 'Saint-Vincent-et-les-Grenadines',
            'SH' => 'Sainte-Hélène',
            'LC' => 'Sainte-Lucie',
            'SV' => 'Salvador',
            'WS' => 'Samoa',
            'AS' => 'Samoa américaines',
            'ST' => 'Sao Tomé-et-Principe',
            'SN' => 'Sénégal',
            'RS' => 'Serbie ',
            'SC' => 'Seychelles',
            'SL' => 'Sierra Leone',
            'SG' => 'Singapour',
            'SK' => 'Slovaquie',
            'SI' => 'Slovénie',
            'SO' => 'Somalie',
            'SD' => 'Soudan',
            'LK' => 'Sri Lanka',
            'SE' => 'Suède',
            'CH' => 'Suisse',
            'SR' => 'Suriname',
            'SZ' => 'Swaziland',
            'SY' => 'Syrie',
            'TJ' => 'Tadjikistan',
            'TW' => 'Taïwan',
            'TZ' => 'Tanzanie',
            'TD' => 'Tchad',
            'TF' => 'Terres australes françaises',
            'IO' => 'Territoire britannique de l\'Océan Indien',
            'TH' => 'Thaïlande',
            'TL' => 'Timor Oriental',
            'TG' => 'Togo',
            'TK' => 'Tokélaou',
            'TO' => 'Tonga',
            'TT' => 'Trinité-et-Tobago',
            'TN' => 'Tunisie',
            'TM' => 'Turkménistan',
            'TR' => 'Turquie',
            'TV' => 'Tuvalu',
            'UA' => 'Ukraine',
            'UY' => 'Uruguay',
            'VU' => 'Vanuatu',
            'VE' => 'Venezuela',
            'VN' => 'Viêt Nam',
            'WF' => 'Wallis-et-Futuna',
            'YE' => 'Yémen',
            'ZM' => 'Zambie',
            'ZW' => 'Zimbabwe ZW',
        ];

        return $nationalities;
    }

    /**
     * @return array<int, string>
     */
    public static function getIsCompetitorChoices()
    {
        return [
            0 => 'Non, les compétitions ne m\'intéressent pas',
            1 => 'Après tout, pourquoi pas !',
            2 => 'Oui, les compétitions m\'intéressent',
        ];
    }

    /**
     * @return array<int, string>
     */
    public static function getStatusChoices()
    {
        return [
            self::STATUS_WAITING => 'En attente',
            self::STATUS_ACTIVE => 'Actif',
            self::STATUS_INACTIVE => 'Inactif',
            self::STATUS_PAYMENT_PENDING => 'Attente paiement',
            self::STATUS_WAITING_PLACE => 'Attente place',
        ];
    }

    public function getStatusLabel(): string
    {
        return self::getStatusChoices()[$this->status];
    }

    /**
     * @return array<int, string>
     */
    public static function getCertificateStatusChoices()
    {
        return [
            self::CERTIFICATE_ABSENT => 'Non transmis',
            self::CERTIFICATE_WAITING => 'À valider',
            self::CERTIFICATE_VALID => 'Validé',
        ];
    }

    public function getCertifStatusLabel(): string
    {
        return self::getCertificateStatusChoices()[$this->certificateStatus];
    }

    /**
     * @return array<int, string>
     */
    public static function getCertificateOldChoices(string $year = "cette année")
    {
        return [
            0 => 'Certificat datant de ' . $year,
            1 => 'Attestation avec toutes cases à non',
        ];
    }

    /*  0 => 'Certificat datant de ' . $this->season->getBeginAt()->format('Y')), */
    /*  0 => 'Certificat datant de ' . getSeason()->getBeginAt()->format('Y')), */
    #[ORM\PreFlush]
    public function calculatePriceAndPaid(): void
    {
        if ($this->season instanceof Season) {
            $this->setPriceCalculated($this->season->getSubscriptionPrice($this));
        }
        $this->paidTotal = $this->getPaidTotal();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setSeason(Season $season = null): self
    {
        $this->season = $season;

        return $this;
    }

    public function getSeason(): ?Season
    {
        return $this->season;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setBirthName(?string $birthName = null): self
    {
        $this->birthName = $birthName;

        return $this;
    }

    public function getBirthName(): ?string
    {
        return $this->birthName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setSex(string $sex): self
    {
        $this->sex = $sex;

        return $this;
    }

    public function getSex(): ?string
    {
        return $this->sex;
    }

    public function setGoesAlone(bool $goesAlone): self
    {
        $this->goesAlone = $goesAlone;

        return $this;
    }

    public function getGoesAlone(): bool
    {
        return $this->goesAlone;
    }

    public function setIsRenewal(bool $isRenewal): self
    {
        $this->isRenewal = $isRenewal;

        return $this;
    }

    public function getIsRenewal(): ?bool
    {
        return $this->isRenewal;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setActivatedAt(?DateTimeInterface $activatedAt): self
    {
        $this->activatedAt = $activatedAt;

        return $this;
    }

    public function getActivatedAt(): ?DateTimeInterface
    {
        return $this->activatedAt;
    }

    public function setActivatedBy(?string $activatedBy): self
    {
        $this->activatedBy = $activatedBy;

        return $this;
    }

    public function getActivatedBy(): ?string
    {
        return $this->activatedBy;
    }

    public function setBirthday(DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        if ($this->season instanceof Season) {
            $limit = new DateTime($this->season->getBeginAt()->format('Y-10-01'));
            $this->age = (int) $limit->diff($birthday)->format('%Y');
        }

        return $this;
    }

    public function getBirthday(): ?DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthPlace(string $birthPlace): self
    {
        $this->birthPlace = $birthPlace;

        return $this;
    }

    public function getBirthPlace(): ?string
    {
        return $this->birthPlace;
    }

    public function setAge(float $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getAge(): ?float
    {
        return $this->age;
    }

    public function setOccupation(string $occupation): self
    {
        $this->occupation = $occupation;

        return $this;
    }

    public function getOccupation(): ?string
    {
        return $this->occupation;
    }

    public function setLicence(?string $licence = null): self
    {
        $this->licence = $licence;

        return $this;
    }

    public function getLicence(): ?string
    {
        return $this->licence;
    }

    public function setCertificateFile(?string $certificateFile): self
    {
        $this->certificateFile = $certificateFile;

        return $this;
    }

    public function getCertificateFile(): ?string
    {
        return $this->certificateFile;
    }

    public function setCertificateDoctor(?string $certificateDoctor): self
    {
        $this->certificateDoctor = $certificateDoctor;

        return $this;
    }

    public function getCertificateDoctor(): ?string
    {
        return $this->certificateDoctor;
    }

    public function setCertificateDate(?DateTimeInterface $certificateDate): self
    {
        $this->certificateDate = $certificateDate;

        return $this;
    }

    public function getCertificateDate(): ?DateTimeInterface
    {
        return $this->certificateDate;
    }

    public function setCertificateOld(bool $certificateOld): self
    {
        $this->certificateOld = $certificateOld;

        return $this;
    }

    public function getCertificateOld(): ?bool
    {
        return $this->certificateOld;
    }

    public function __toString(): string
    {
        return sprintf('%s %s %s', $this->firstName, $this->lastName, $this->season instanceof Season ? '(' . $this->season->getTitle() . ')' : '');
    }

    public function setUserId(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function setSlot1(Slot $slot1 = null): self
    {
        $this->slot1 = $slot1;

        return $this;
    }

    public function getSlot1(): ?Slot
    {
        return $this->slot1;
    }

    public function setSlot2(Slot $slot2 = null): self
    {
        $this->slot2 = $slot2;

        return $this;
    }

    public function getSlot2(): ?Slot
    {
        return $this->slot2;
    }

    public function setActivatedSlot(Slot $activatedSlot = null): self
    {
        $this->activatedSlot = $activatedSlot;

        return $this;
    }

    public function getActivatedSlot(): ?Slot
    {
        return $this->activatedSlot;
    }

    private function filterPhone(string $value): string
    {
        $result = preg_replace('/[^0-9]/', '', $value);

        if (is_null($result)) {
            return '';
        }

        return $result;
    }

    // Validation
    #[Assert\Callback]
    public function validateSlots(ExecutionContextInterface $context): void
    {
        if ($this->noSlot) {
            $this->slot1 = $this->slot2 = null;
        } elseif (!$this->slot1 instanceof Slot || !$this->slot2 instanceof Slot) {
            $context->addViolation('Vous devez choisir 2 créneaux différents', []);
        } elseif ($this->slot1->getId() === $this->slot2->getId()) {
            $context->addViolation('Vous devez choisir 2 créneaux différents', []);
        }
    }

    #[Assert\Callback]
    public function validateCertificate(ExecutionContextInterface $context): void
    {
        // Si on est en renouvellement  et que l'on a un certificat de moins de 3 ans, aucun besoin des autres champs et seulement envoyer valeur ATTESTATION=OUI en base
        // Sinon tous les controles s'appliquent

        if ($this->certificateOld) {
            return;
        }

        // Date du certificat bien saisie
        /* if (null !== $this->certificateDate) {
            $minDate = clone $this->season->getBeginAt();
            $minDate->modify('-9 months');
            $maxDate = new \DateTime();

            if ($this->certificateDate > $maxDate) {
            //    $context->addViolation('La date du certificat médical est dans le futur.', array(), null);
            } elseif ($this->certificateDate < $minDate) {
                $context->addViolation('La date du certificat médical est trop ancienne (max 1 an au 1er septembre). Sélectionnez le champs «Certificat de plus d\'un an» si vous avez renseigné l\'autoquestionnaire de santé QS SPORT.', array());
            }
        } */

        // if (self::CERTIFICATE_VALID !== $this->certificateStatus) {
        //    return;
        // }

        // if ($this->certificateDoctor === null) {
        //     $context->addViolation('Le nom du médecin doit être saisi pour valider le certificat médical', []);
        // }

        // if (null === $this->certificateDate) {
        //    $context->addViolation('La date doit être saisie pour valider le certificat médical', array());
        // }
    }

    #[Assert\Callback]
    public function validateComplete(ExecutionContextInterface $context): void
    {
        if ($this->status !== self::STATUS_ACTIVE) {
            return;
        }

        if ($this->noSlot) {
            $this->activatedSlot = null;
        } elseif (!$this->activatedSlot instanceof Slot) {
            $context->addViolation('Vous devez choisir un créneau pour valider une inscription', []);
        }

        if ($this->getPaidTotal() < $this->getPriceCorrected()) {
            $context->addViolation('Le montant total payé est insuffisant pour valider une inscription', []);
        }
    }

    public function setCertificateCompetition(bool $certificateCompetition): self
    {
        $this->certificateCompetition = $certificateCompetition;

        return $this;
    }

    public function getCertificateCompetition(): bool
    {
        return $this->certificateCompetition;
    }

    public function setNoSlot(bool $noSlot): self
    {
        $this->noSlot = $noSlot;

        return $this;
    }

    public function getNoSlot(): bool
    {
        return $this->noSlot;
    }

    public function setInsurance(int $insurance): self
    {
        $this->insurance = $insurance;

        return $this;
    }

    public function getInsurance(): int
    {
        return $this->insurance;
    }

    public function setInsuranceIj(int $insuranceIj): self
    {
        $this->insuranceIj = $insuranceIj;

        return $this;
    }

    public function getInsuranceIj(): int
    {
        return $this->insuranceIj;
    }

    public function setInsuranceSlackline(int $insuranceSlackline): self
    {
        $this->insuranceSlackline = $insuranceSlackline;

        return $this;
    }

    public function getInsuranceSlackline(): int
    {
        return $this->insuranceSlackline;
    }

    public function setInsuranceSki(int $insuranceSki): self
    {
        $this->insuranceSki = $insuranceSki;

        return $this;
    }

    public function getInsuranceSki(): int
    {
        return $this->insuranceSki;
    }

    public function setPayment3Times(bool $payment3Times): self
    {
        $this->payment3Times = $payment3Times;

        return $this;
    }

    public function getPayment3Times(): bool
    {
        return $this->payment3Times;
    }

    public function setParentLastName(?string $parentLastName): self
    {
        $this->parentLastName = $parentLastName;

        return $this;
    }

    public function getParentLastName(): ?string
    {
        return $this->parentLastName;
    }

    public function setParentFirstName(?string $parentFirstName): self
    {
        $this->parentFirstName = $parentFirstName;

        return $this;
    }

    public function getParentFirstName(): ?string
    {
        return $this->parentFirstName;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $this->filterPhone($phone);

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setParentPhone(?string $parentPhone): self
    {
        if ($parentPhone) {
            $this->parentPhone = $this->filterPhone($parentPhone);
        }

        return $this;
    }

    public function getParentPhone(): ?string
    {
        return $this->parentPhone;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setClimbingLevel(string $climbingLevel): self
    {
        $this->climbingLevel = $climbingLevel;

        return $this;
    }

    public function getClimbingLevel(): ?string
    {
        return $this->climbingLevel;
    }

    public function setClimbingExperience(string $climbingExperience): self
    {
        $this->climbingExperience = $climbingExperience;

        return $this;
    }

    public function getClimbingExperience(): ?string
    {
        return $this->climbingExperience;
    }

    public function setPassportFfme(string $passportFfme): self
    {
        $this->passportFfme = $passportFfme;

        return $this;
    }

    public function getPassportFfme(): ?string
    {
        return $this->passportFfme;
    }

    public function setIsCompetitor(int $isCompetitor): self
    {
        $this->isCompetitor = $isCompetitor;

        return $this;
    }

    public function getIsCompetitor(): int
    {
        return $this->isCompetitor;
    }

    public function setParentEmail(?string $parentEmail): self
    {
        $this->parentEmail = $parentEmail;

        return $this;
    }

    public function getParentEmail(): ?string
    {
        return $this->parentEmail;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    public function isInactive(): bool
    {
        return $this->status === self::STATUS_INACTIVE;
    }

    public function isWaiting(): bool
    {
        return $this->status === self::STATUS_WAITING;
    }

    public function isWaitingPlace(): bool
    {
        return $this->status === self::STATUS_WAITING_PLACE;
    }

    public function isPaymentPending(): bool
    {
        return $this->status === self::STATUS_PAYMENT_PENDING;
    }

    public function hasValidCertificate(): bool
    {
        return $this->certificateStatus === self::CERTIFICATE_VALID;
    }

    public function setCertificateStatus(int $certificateStatus): self
    {
        $this->certificateStatus = $certificateStatus;

        return $this;
    }

    public function getCertificateStatus(): int
    {
        return $this->certificateStatus;
    }

    public function setPaymentPass92(bool $paymentPass92): self
    {
        $this->paymentPass92 = $paymentPass92;

        return $this;
    }

    public function getPaymentPass92(): bool
    {
        return $this->paymentPass92;
    }

    public function setPaymentCouponSport(bool $paymentCouponSport): self
    {
        $this->paymentCouponSport = $paymentCouponSport;

        return $this;
    }

    public function getPaymentCouponSport(): bool
    {
        return $this->paymentCouponSport;
    }

    public function setPaymentSubvensionCe(bool $paymentSubvensionCe): self
    {
        $this->paymentSubvensionCe = $paymentSubvensionCe;

        return $this;
    }

    public function getPaymentSubvensionCe(): bool
    {
        return $this->paymentSubvensionCe;
    }

    public function setPriceCalculated(float $priceCalculated): self
    {
        $this->priceCalculated = $priceCalculated;

        return $this;
    }

    public function getPriceCalculated(): float
    {
        return $this->priceCalculated;
    }

    public function setPriceCorrected(float $priceCorrected): self
    {
        $this->priceCorrected = $priceCorrected;

        return $this;
    }

    public function getPriceCorrected(): float
    {
        return $this->priceCorrected;
    }

    public function getPrice(): float
    {
        return $this->getPriceCorrected() ?: $this->getPriceCalculated();
    }

    public function setPaidTotal(float $paidTotal): self
    {
        $this->paidTotal = $paidTotal;

        return $this;
    }

    public function getPaidTotal(): float
    {
        $this->paidTotal = $this->paidCash
        + $this->paidCB
        + $this->paidPass92
        + $this->paidCouponSport
        + $this->paidSubvensionCe
        + $this->paidRegul
        + $this->paidCheck01
        + $this->paidCheck02
        + $this->paidCheck03
        + $this->paidCheckGarantee
        ;

        return $this->paidTotal;
    }

    public function setPaidCash(float $paidCash): self
    {
        $this->paidCash = $paidCash;

        return $this;
    }

    public function getPaidCash(): float
    {
        return $this->paidCash;
    }

    public function setPaidCB(float $paidCB): self
    {
        $this->paidCB = $paidCB;

        return $this;
    }

    public function getPaidCB(): float
    {
        return $this->paidCB;
    }

    public function setPaidPass92(float $paidPass92): self
    {
        $this->paidPass92 = $paidPass92;

        return $this;
    }

    public function getPaidPass92(): float
    {
        return $this->paidPass92;
    }

    public function setPaidCouponSport(float $paidCouponSport): self
    {
        $this->paidCouponSport = $paidCouponSport;

        return $this;
    }

    public function getPaidCouponSport(): float
    {
        return $this->paidCouponSport;
    }

    public function setPaidSubvensionCe(float $paidSubvensionCe): self
    {
        $this->paidSubvensionCe = $paidSubvensionCe;

        return $this;
    }

    public function getPaidSubvensionCe(): float
    {
        return $this->paidSubvensionCe;
    }

    public function setPaidRegul(float $paidRegul): self
    {
        $this->paidRegul = $paidRegul;

        return $this;
    }

    public function getPaidRegul(): float
    {
        return $this->paidRegul;
    }

    public function setPaidCheck01(float $paidCheck01): self
    {
        $this->paidCheck01 = $paidCheck01;

        return $this;
    }

    public function getPaidCheck01(): float
    {
        return $this->paidCheck01;
    }

    public function setPaidCheck02(float $paidCheck02): self
    {
        $this->paidCheck02 = $paidCheck02;

        return $this;
    }

    public function getPaidCheck02(): float
    {
        return $this->paidCheck02;
    }

    public function setPaidCheck03(float $paidCheck03): self
    {
        $this->paidCheck03 = $paidCheck03;

        return $this;
    }

    public function getPaidCheck03(): float
    {
        return $this->paidCheck03;
    }

    public function setKey(string $key): self
    {
        $this->key = $key;

        return $this;
    }

    public function getKey(): ?string
    {
        return $this->key;
    }

    public function setPaidCheckGarantee(float $paidCheckGarantee): self
    {
        $this->paidCheckGarantee = $paidCheckGarantee;

        return $this;
    }

    public function getPaidCheckGarantee(): float
    {
        return $this->paidCheckGarantee;
    }

    public function setLicenceFamily(bool $licenceFamily): self
    {
        $this->licenceFamily = $licenceFamily;

        return $this;
    }

    public function getLicenceFamily(): bool
    {
        return $this->licenceFamily;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setNationality(string $nationality): self
    {
        $this->nationality = $nationality;

        return $this;
    }

    public function getNationality(): ?string
    {
        return $this->nationality;
    }

    public function setLicenceDate(?DateTimeInterface $licenceDate): self
    {
        $this->licenceDate = $licenceDate;

        return $this;
    }

    public function getLicenceDate(): ?DateTimeInterface
    {
        return $this->licenceDate;
    }
}
