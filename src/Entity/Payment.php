<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use App\Repository\PaymentRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use stdClass;

/**
 * Slot
 */
#[ORM\Entity(repositoryClass: PaymentRepository::class)]
#[ORM\Table(name: 'wp_app_payment')]
class Payment
{
    #[ORM\Column(name: 'id', type: Types::INTEGER)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id = null;

    // L’identifiant du paiement
    #[ORM\Column(name: 'payment_id', type: Types::STRING, nullable: false)]
    private ?string $paymentId = null;

    // La date du paiement
    #[ORM\Column(name: 'date', type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $date = null;

    // Le montant du paiement
    #[ORM\Column(name: 'amount', type: Types::FLOAT, nullable: true)]
    private ?float $amount = null;

    // Type de paiement paiement
    #[ORM\Column(name: 'type', type: Types::STRING, nullable: true)]
    private ?string $type = null;

    // L’url de la campagne sur laquelle a été effectué le paiement
    #[ORM\Column(name: 'url', type: Types::STRING, nullable: true)]
    private ?string $url = null;

    #[ORM\Column(name: 'payer_first_name', type: Types::STRING, nullable: true)]
    private ?string $payerFirstName = null;

    #[ORM\Column(name: 'payer_last_name', type: Types::STRING, nullable: true)]
    private ?string $payerLastName = null;

    // L’url du reçu
    #[ORM\Column(name: 'url_receipt', type: Types::STRING, nullable: true)]
    private ?string $urlReceipt = null;

    // L’url du reçu fiscal
    #[ORM\Column(name: 'url_tax_receipt', type: Types::STRING, nullable: true)]
    private ?string $urlTaxReceipt = null;

    // Action ID à requeter pour les infos complémentaires
    #[ORM\Column(name: 'action_id', type: Types::STRING, nullable: true)]
    private ?string $actionId = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPaymentId(): ?string
    {
        return $this->paymentId;
    }

    public function setPaymentId(string $paymentId): self
    {
        $this->paymentId = $paymentId;

        return $this;
    }

    public function getDate(): ?DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getPayerFirstName(): ?string
    {
        return $this->payerFirstName;
    }

    public function setPayerFirstName(string $payerFirstName): self
    {
        $this->payerFirstName = $payerFirstName;

        return $this;
    }

    public function getPayerLastName(): ?string
    {
        return $this->payerLastName;
    }

    public function setPayerLastName(string $payerLastName): self
    {
        $this->payerLastName = $payerLastName;

        return $this;
    }

    public function getUrlReceipt(): ?string
    {
        return $this->urlReceipt;
    }

    public function setUrlReceipt(string $urlReceipt): self
    {
        $this->urlReceipt = $urlReceipt;

        return $this;
    }

    public function getUrlTaxReceipt(): ?string
    {
        return $this->urlTaxReceipt;
    }

    public function setUrlTaxReceipt(string $urlTaxReceipt): self
    {
        $this->urlTaxReceipt = $urlTaxReceipt;

        return $this;
    }

    public function getActionId(): ?string
    {
        return $this->actionId;
    }

    public function setActionId(string $actionId): self
    {
        $this->actionId = $actionId;

        return $this;
    }

    public function fromJson(stdClass $json): void
    {
        $this->setPaymentId($json->id);
        $this->setDate(new DateTime($json->date));
        $this->setAmount($json->amount);
        $this->setType($json->type);
        $this->setUrl($json->url);
        $this->setPayerFirstName($json->payer_first_name);
        $this->setPayerLastName($json->payer_last_name);
        $this->setUrlReceipt($json->url_receipt);
        $this->setUrlTaxReceipt($json->url_tax_receipt);
        $this->setActionId($json->action_id);
    }
}

//@param array{id: string, date: string, amount: float, type: string, url: string, payer_first_name: string, payer_last_name: string, url_receipt: string, url_tax_receipt: string, action_id: string } $json
