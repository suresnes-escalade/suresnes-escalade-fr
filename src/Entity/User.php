<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use App\Repository\UserRepository;
use Stringable;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User
 */
#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: 'wp_users')]
#[ORM\Index(name: 'user_login_key', columns: ['user_login'])]
#[ORM\Index(name: 'user_nicename', columns: ['user_nicename'])]
#[ORM\Index(name: 'user_email', columns: ['user_email'])]
class User implements UserInterface, EquatableInterface, Stringable, PasswordAuthenticatedUserInterface
{
    public const WP_META_KEY_ROLES = 'wp_capabilities';

    #[ORM\Column(name: 'ID', type: Types::BIGINT)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private int $id;

    /**
     * @var Collection<int, UserMeta> $metas
     */
    #[ORM\OneToMany(targetEntity: UserMeta::class, mappedBy: 'user', cascade: ['persist', 'remove'])]
    private Collection $metas;

    #[ORM\Column(name: 'user_login', type: Types::STRING, length: 60)]
    private string $login = '';

    #[ORM\Column(name: 'user_pass', type: Types::STRING, length: 64)]
    private string $pass = '';

    #[ORM\Column(name: 'user_nicename', type: Types::STRING, length: 50)]
    private string $nicename = '';

    #[ORM\Column(name: 'user_email', type: Types::STRING, length: 100)]
    private string $email = '';

    #[ORM\Column(name: 'user_url', type: Types::STRING, length: 100)]
    private string $url = '';

    #[ORM\Column(name: 'user_registered', type: Types::DATETIME_MUTABLE)]
    private DateTimeInterface $registered;

    #[ORM\Column(name: 'user_activation_key', type: Types::STRING, length: 60)]
    private string $activationKey = '';

    #[ORM\Column(name: 'user_status', type: Types::INTEGER, length: 11)]
    private int $status = 0;

    #[ORM\Column(name: 'display_name', type: Types::STRING, length: 250)]
    private string $displayName = '';

    /**
     * @var array<string, bool> $allcaps
     */
    private array $allcaps = [
        'level_10' => true,
        'level_9' => true,
        'level_8' => true,
        'level_7' => true,
        'level_6' => true,
        'level_5' => true,
        'level_4' => true,
        'level_3' => true,
        'level_2' => true,
        'level_1' => true,
        'level_0' => true
    ];

    /**
     * @var array<string, string> $wpRoles
     */
    private array $wpRoles = [
        'administrator' => 'Administrateur',
        'editor' => 'Editeur',
        'author' => 'Auteur',
        'contributor' => 'Contributeur',
        'subscriber' => 'Abonné'
    ];

    /**
     * @var array<string> $roles
     */
    private array $roles;

    public function __construct()
    {
        $this->metas = new ArrayCollection();
        $this->registered = new DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setActivationKey(string $activationKey): self
    {
        $this->activationKey = $activationKey;

        return $this;
    }

    public function getActivationKey(): string
    {
        return $this->activationKey;
    }

    public function setDisplayName(string $displayName): self
    {
        $this->displayName = $displayName;

        return $this;
    }

    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setLogin(string $login): self
    {
        $this->login = $login;

        return $this;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function setNicename(string $nicename): self
    {
        $this->nicename = $nicename;

        return $this;
    }

    public function getNicename(): string
    {
        return $this->nicename;
    }

    public function setPass(string $pass): self
    {
        $this->pass = $pass;

        return $this;
    }

    public function getPass(): string
    {
        return $this->pass;
    }

    public function setRegistered(DateTimeInterface $registered): self
    {
        $this->registered = $registered;

        return $this;
    }

    public function getRegistered(): DateTimeInterface
    {
        return $this->registered;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param Collection<int, UserMeta> $metas
     */
    public function setMetas(Collection $metas): self
    {
        $this->metas = $metas;

        return $this;
    }

    /**
     * Returns user meta value from a meta key name
     */
    public function getMetaValue(string $name): mixed
    {
        foreach ($this->getMetas() as $meta) {
            if ($name == $meta->getKey()) {
                return $meta->getValue();
            }
        }

        return null;
    }

    /**
     * @return Collection<int, UserMeta>
     */
    public function getMetas(): Collection
    {
        return $this->metas;
    }

    public function setRoles(mixed $roles): self
    {
        if (!is_array($roles)) {
            $roles = [$roles];
        }

        $this->roles = $roles;

        return $this;
    }

    /**
     * Sets Wordpress user roles by prefixing them
     *
     * @param array<string> $roles
     */
    public function setWordpressRoles(array $roles, string $prefix = 'ROLE_WP_'): self
    {
        foreach ($roles as $key => $role) {
            $user_meta = new UserMeta();
            $user_meta->setKey('wp_capabilities');
            $user_meta->setValue([strtolower((string) $role) => true]);
            $user_meta->setUser($this);
            $this->metas[] = $user_meta;

            $user_meta = new UserMeta();
            $user_meta->setKey('wp_user_level');
            $user_meta->setValue(array_reduce(array_keys($this->allcaps), [$this, 'levelReduction'], 0));
            $user_meta->setUser($this);
            $this->metas[] = $user_meta;

            $roles[$key] = $prefix . strtoupper((string) $role);
        }

        $this->setRoles($roles);

        return $this;
    }

    /**
     * @see wp-includes/class-wp-user.php
     */
    private function levelReduction(int $max, string $item): int
    {
        if (preg_match('/^level_(10|[0-9])$/i', $item, $matches)) {
            $level = intval($matches[1]);

            return max($max, $level);
        } else {
            return $max;
        }
    }

    public function getRole(): string
    {
        $capabilities = $this->getMetas()->filter(fn ($meta) => $meta->getKey() === 'wp_capabilities');

        if (!$capabilities->get(0) || !$capabilities->get(0)->getValue()) {
            return '';
        }

        $_value = unserialize($capabilities->get(0)->getValue());
        if (!is_array($_value)) {
            return '';
        }

        return $this->wpRoles[array_keys($_value)[0]];
    }

    public function getRoles(): array
    {
        return $this->roles ?: [];
    }

    public function getPassword(): string
    {
        return $this->getPass();
    }

    public function getUserIdentifier(): string
    {
        return $this->getLogin();
    }

    public function eraseCredentials(): void
    {
    }

    public function __toString(): string
    {
        return $this->getUserIdentifier();
    }

    public function isEqualTo(UserInterface $user): bool
    {
        if (!$user instanceof User) {
            return false;
        }

        if ($this->pass !== $user->getPass()) {
            return false;
        }
        return $this->login === $user->getLogin();
    }
}
