<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use App\Repository\SeasonRepository;
use Stringable;
use UnexpectedValueException;
use DateTimeInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Season
 */
#[ORM\Entity(repositoryClass: SeasonRepository::class)] // tarifs:
#[ORM\Table(name: 'wp_app_season')]
class Season implements Stringable
{
    #[ORM\Column(name: 'id', type: Types::INTEGER)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id = null;

    #[ORM\Column(name: 'title', type: Types::STRING, length: 50)]
    private string $title;

    #[ORM\Column(name: 'beginAt', type: Types::DATETIME_MUTABLE)]
    private DateTimeInterface $beginAt;

    #[ORM\Column(name: 'endAt', type: Types::DATETIME_MUTABLE)]
    private DateTimeInterface $endAt;

    /**
     * @var Collection<int, Slot>
     */
    #[ORM\OneToMany(targetEntity: Slot::class, mappedBy: 'season', cascade: ['all'], orphanRemoval: false)]
    #[ORM\OrderBy(['dayOfWeek' => 'ASC', 'beginTime' => 'ASC'])]
    private Collection $slots;

    /**
     * @var float Prix "Licence Famille"
     */
    #[Assert\NotBlank]
    #[Assert\Range(min: 0, max: 500)]
    #[Assert\Type(type: 'float')]
    #[ORM\Column(name: 'price_licence_child', type: Types::FLOAT, nullable: false)]
    private float $priceLicenceChild;

    /**
     * @var float Prix "Licence Famille"
     */
    #[Assert\NotBlank]
    #[Assert\Range(min: 0, max: 500)]
    #[Assert\Type(type: 'float')]
    #[ORM\Column(name: 'price_licence_adult', type: Types::FLOAT, nullable: false)]
    private float $priceLicenceAdult;

    /**
     * @var float Prix "Licence Famille" (à partir de 3 personnes)
     */
    #[Assert\NotBlank]
    #[Assert\Range(min: 0, max: 500)]
    #[Assert\Type(type: 'float')]
    #[ORM\Column(name: 'price_licence_family', type: Types::FLOAT, nullable: false)]
    private float $priceLicenceFamily;

    /**
     * @var float Prix "Première inscription" (frais de dossier)
     */
    #[Assert\NotBlank]
    #[Assert\Range(min: 0, max: 500)]
    #[Assert\Type(type: 'float')]
    #[ORM\Column(name: 'price_first_registration', type: Types::FLOAT, nullable: false)]
    private float $priceFirstRegistration;

    /**
     * @var float Prix "Enfant Non-Suresnois"
     */
    #[Assert\NotBlank]
    #[Assert\Range(min: 0, max: 500)]
    #[Assert\Type(type: 'float')]
    #[ORM\Column(name: 'price_not_resident_child', type: Types::FLOAT, nullable: false)]
    private float $priceNotResidentChild;

    /**
     * @var float Prix "Adulte Non-Suresnois"
     */
    #[Assert\NotBlank]
    #[Assert\Range(min: 0, max: 500)]
    #[Assert\Type(type: 'float')]
    #[ORM\Column(name: 'price_not_resident_adult', type: Types::FLOAT, nullable: false)]
    private float $priceNotResidentAdult;

    // Additions
    /**
     * @var float Prix "Part CD + CR"
     */
    #[Assert\NotBlank]
    #[Assert\Range(min: 0, max: 500)]
    #[Assert\Type(type: 'float')]
    #[ORM\Column(name: 'price_committee', type: Types::FLOAT, nullable: false)]
    private float $priceCommittee;

    /**
     * @var float Prix "Part club enfant"
     */
    #[Assert\NotBlank]
    #[Assert\Range(min: 0, max: 500)]
    #[Assert\Type(type: 'float')]
    #[ORM\Column(name: 'price_club_child', type: Types::FLOAT, nullable: false)]
    private float $priceClubChild;

    /**
     * @var float Prix "Part club adulte"
     */
    #[Assert\NotBlank]
    #[Assert\Range(min: 0, max: 500)]
    #[Assert\Type(type: 'float')]
    #[ORM\Column(name: 'price_club_adult', type: Types::FLOAT, nullable: false)]
    private float $priceClubAdult;

    /**
     * @var float Prix "Sans assurance"
     */
    #[Assert\NotBlank]
    #[Assert\Range(min: 0, max: 500)]
    #[Assert\Type(type: 'float')]
    #[ORM\Column(name: 'price_insurance0', type: Types::FLOAT, nullable: false)]
    private float $priceInsurance0;

    /**
     * @var float Prix "Assurance base"
     */
    #[Assert\NotBlank]
    #[Assert\Range(min: 0, max: 500)]
    #[Assert\Type(type: 'float')]
    #[ORM\Column(name: 'price_insurance1', type: Types::FLOAT, nullable: false)]
    private float $priceInsurance1;

    /**
     * @var float Prix "Assurance base +"
     */
    #[Assert\NotBlank]
    #[Assert\Range(min: 0, max: 500)]
    #[Assert\Type(type: 'float')]
    #[ORM\Column(name: 'price_insurance2', type: Types::FLOAT, nullable: false)]
    private float $priceInsurance2;

    /**
     * @var float Prix "Assurance base ++"
     */
    #[Assert\NotBlank]
    #[Assert\Range(min: 0, max: 500)]
    #[Assert\Type(type: 'float')]
    #[ORM\Column(name: 'price_insurance3', type: Types::FLOAT, nullable: false)]
    private float $priceInsurance3;

    /**
     * @var float Prix "Assurance option Slackline"
     */
    #[Assert\NotBlank]
    #[Assert\Range(min: 0, max: 500)]
    #[Assert\Type(type: 'float')]
    #[ORM\Column(name: 'price_insurance_slackline', type: Types::FLOAT, nullable: false)]
    private float $priceInsuranceSlackline;

    /**
     * @var float Prix "Assurance option Ski"
     */
    #[Assert\NotBlank]
    #[Assert\Range(min: 0, max: 500)]
    #[Assert\Type(type: 'float')]
    #[ORM\Column(name: 'price_insurance_ski', type: Types::FLOAT, nullable: false)]
    private float $priceInsuranceSki;

    /**
     * @var float Prix "Assurance option IJ 1"
     */
    #[Assert\NotBlank]
    #[Assert\Range(min: 0, max: 500)]
    #[Assert\Type(type: 'float')]
    #[ORM\Column(name: 'price_options_ij1', type: Types::FLOAT, nullable: false)]
    private float $priceOptionIJ1;

    /**
     * @var float Prix "Assurance option IJ 2"
     */
    #[Assert\NotBlank]
    #[Assert\Range(min: 0, max: 500)]
    #[Assert\Type(type: 'float')]
    #[ORM\Column(name: 'price_options_ij2', type: Types::FLOAT, nullable: false)]
    private float $priceOptionIJ2;

    /**
     * @var float Prix "Assurance option IJ 3"
     */
    #[Assert\NotBlank]
    #[Assert\Range(min: 0, max: 500)]
    #[Assert\Type(type: 'float')]
    #[ORM\Column(name: 'price_options_ij3', type: Types::FLOAT, nullable: false)]
    private float $priceOptionIJ3;

    public function __construct()
    {
        $this->slots = new ArrayCollection();
    }

    /**
     * Compute the subscription price
     *
     * @return float
     *
     *
     * calcul = licence
     *     + [part CR+CD]
     *     + [assurance]
     *     + [options IJ]
     *     + [option SLACKLINE]
     *     + [option SKI]
     *     + [part club]
     *     + [surcout non-suresnois]
     *     + [surcout 1er inscription]
     */
    public function getSubscriptionPrice(Subscription $subscription): float
    {
        $isChild = $subscription->getAge() < 18;

        $price = 0;

        if ($subscription->getLicenceFamily()) {
            $price += $this->priceLicenceFamily;
        } elseif ($isChild) {
            $price += $this->priceLicenceChild;
        } else {
            $price += $this->priceLicenceAdult;
        }

        $price += $isChild ? $this->priceClubChild : $this->priceClubAdult;
        $price += $this->priceCommittee;

        switch ($subscription->getInsurance()) {
            case 0:$price += $this->priceInsurance0;
                break;
            case 1:$price += $this->priceInsurance1;
                break;
            case 2:$price += $this->priceInsurance2;
                break;
            case 3:$price += $this->priceInsurance3;
                break;
        }

        switch ($subscription->getInsuranceIj()) {
            case 1:$price += $this->priceOptionIJ1;
                break;
            case 2:$price += $this->priceOptionIJ2;
                break;
            case 3:$price += $this->priceOptionIJ3;
                break;
        }

        if ($subscription->getInsuranceSlackline() !== 0) {
            $price += $this->priceInsuranceSlackline;
        }

        if ($subscription->getInsuranceSki() !== 0) {
            $price += $this->priceInsuranceSki;
        }

        if (!$subscription->getIsRenewal()) {
            $price += $this->priceFirstRegistration;
        }

        if ($subscription->getCity() && strtoupper(trim($subscription->getCity())) !== 'SURESNES') {
            $price += $isChild ? $this->priceNotResidentChild : $this->priceNotResidentAdult;
        }

        if ($price <= 0) {
            throw new UnexpectedValueException('The price cannot be Zero');
        }

        return $price;
    }

    public function __toString(): string
    {
        return $this->title;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setBeginAt(DateTimeInterface $beginAt): self
    {
        $this->beginAt = $beginAt;

        return $this;
    }

    public function getBeginAt(): DateTimeInterface
    {
        return $this->beginAt;
    }

    public function setEndAt(DateTimeInterface $endAt): self
    {
        $this->endAt = $endAt;

        return $this;
    }

    public function getEndAt(): DateTimeInterface
    {
        return $this->endAt;
    }

    public function addSlot(Slot $slot): self
    {
        $this->slots[] = $slot;

        return $this;
    }

    public function removeSlot(Slot $slot): void
    {
        $this->slots->removeElement($slot);
    }

    /**
     * @return Collection<int, Slot>
     */
    public function getSlots(): Collection
    {
        return $this->slots;
    }

    /**
     * @return array<int, Slot>
     */
    public function getSlotsForSubscription(Subscription $subscription): array
    {
        $slots = [];

        $birthday = $subscription->getBirthday();
        foreach ($this->getSlots() as $slot) {
            if ($slot->getMaxBirth() != null && $slot->getMaxBirth() > $birthday) {
                continue;
            }

            if ($slot->getMinBirth() != null && $slot->getMinBirth() < $birthday) {
                continue;
            }
            $slots[] = $slot;
        }

        return $slots;
    }

    /**
     * @return array<int, string>
     */
    public function getInsuranceChoices(): array
    {
        return [
            0 => sprintf('Responsabilité civile uniquement (+ %d €)', $this->priceInsurance0),
            1 => sprintf('RC + assurance base (+ %d €)', $this->priceInsurance1),
            2 => sprintf('RC + assurance base+ (+ %d €)', $this->priceInsurance2),
            3 => sprintf('RC + assurance base++ (+ %d €)', $this->priceInsurance3),
            // 0 => 'Refuser les garanties de personnes (pas d\'assurance individuelle accident, rapatriement, frais de recherche secours) : responsabilité civile uniquement (+ 3 €)',
        ];
    }

    /**
     * @return array<int, string>
     */
    public function getInsuranceChoicesForSubscription()
    {
        return $this->getInsuranceChoices();
    }

    /**
     * @return array<int, string>
     */
    public function getInsuranceIjChoices()
    {
        return [
            0 => 'Pas d\'option IJ',
            1 => sprintf('IJ1 (+ %d €)', $this->priceOptionIJ1),
            2 => sprintf('IJ2 (+ %d €)', $this->priceOptionIJ2),
            3 => sprintf('IJ3 (+ %d €)', $this->priceOptionIJ3),
        ];
    }

    /**
     * @return array<int, string>
     */
    public function getInsuranceSlacklineChoices()
    {
        return [
            0 => 'Non',
            1 => sprintf('Oui (+ %d €)', $this->priceInsuranceSlackline),
        ];
    }

    /**
     * @return array<int, string>
     */
    public function getInsuranceSkiChoices()
    {
        return [
            0 => 'Non',
            1 => sprintf('Oui (+ %d €)', $this->priceInsuranceSki),
        ];
    }

    public function setPriceLicenceChild(float $priceLicenceChild): self
    {
        $this->priceLicenceChild = $priceLicenceChild;

        return $this;
    }

    public function getPriceLicenceChild(): float
    {
        return $this->priceLicenceChild;
    }

    public function setPriceLicenceAdult(float $priceLicenceAdult): self
    {
        $this->priceLicenceAdult = $priceLicenceAdult;

        return $this;
    }

    public function getPriceLicenceAdult(): float
    {
        return $this->priceLicenceAdult;
    }

    public function setPriceLicenceFamily(float $priceLicenceFamily): self
    {
        $this->priceLicenceFamily = $priceLicenceFamily;

        return $this;
    }

    public function getPriceLicenceFamily(): float
    {
        return $this->priceLicenceFamily;
    }

    public function setPriceFirstRegistration(float $priceFirstRegistration): self
    {
        $this->priceFirstRegistration = $priceFirstRegistration;

        return $this;
    }

    public function getPriceFirstRegistration(): float
    {
        return $this->priceFirstRegistration;
    }

    public function setPriceNotResidentChild(float $priceNotResidentChild): self
    {
        $this->priceNotResidentChild = $priceNotResidentChild;

        return $this;
    }

    public function getPriceNotResidentChild(): float
    {
        return $this->priceNotResidentChild;
    }

    public function setPriceNotResidentAdult(float $priceNotResidentAdult): self
    {
        $this->priceNotResidentAdult = $priceNotResidentAdult;

        return $this;
    }

    public function getPriceNotResidentAdult(): float
    {
        return $this->priceNotResidentAdult;
    }

    public function setPriceCommittee(float $priceCommittee): self
    {
        $this->priceCommittee = $priceCommittee;

        return $this;
    }

    public function getPriceCommittee(): float
    {
        return $this->priceCommittee;
    }

    public function setPriceClubChild(float $priceClubChild): self
    {
        $this->priceClubChild = $priceClubChild;

        return $this;
    }

    public function getPriceClubChild(): float
    {
        return $this->priceClubChild;
    }

    public function setPriceClubAdult(float $priceClubAdult): self
    {
        $this->priceClubAdult = $priceClubAdult;

        return $this;
    }

    public function getPriceClubAdult(): float
    {
        return $this->priceClubAdult;
    }

    public function setPriceInsurance0(float $priceInsurance0): self
    {
        $this->priceInsurance0 = $priceInsurance0;

        return $this;
    }

    public function getPriceInsurance0(): float
    {
        return $this->priceInsurance0;
    }

    public function setPriceInsurance1(float $priceInsurance1): self
    {
        $this->priceInsurance1 = $priceInsurance1;

        return $this;
    }

    public function getPriceInsurance1(): float
    {
        return $this->priceInsurance1;
    }

    public function setPriceInsurance2(float $priceInsurance2): self
    {
        $this->priceInsurance2 = $priceInsurance2;

        return $this;
    }

    public function getPriceInsurance2(): float
    {
        return $this->priceInsurance2;
    }

    public function setPriceInsurance3(float $priceInsurance3): self
    {
        $this->priceInsurance3 = $priceInsurance3;

        return $this;
    }

    public function getPriceInsurance3(): float
    {
        return $this->priceInsurance3;
    }

    public function setPriceInsuranceSlackline(float $priceInsuranceSlackline): self
    {
        $this->priceInsuranceSlackline = $priceInsuranceSlackline;

        return $this;
    }

    public function getPriceInsuranceSlackline(): float
    {
        return $this->priceInsuranceSlackline;
    }

    public function setPriceInsuranceSki(float $priceInsuranceSki): self
    {
        $this->priceInsuranceSki = $priceInsuranceSki;

        return $this;
    }

    public function getPriceInsuranceSki(): float
    {
        return $this->priceInsuranceSki;
    }

    public function setPriceOptionIJ1(float $priceOptionIJ1): self
    {
        $this->priceOptionIJ1 = $priceOptionIJ1;

        return $this;
    }

    public function getPriceOptionIJ1(): float
    {
        return $this->priceOptionIJ1;
    }

    public function setPriceOptionIJ2(float $priceOptionIJ2): self
    {
        $this->priceOptionIJ2 = $priceOptionIJ2;

        return $this;
    }

    public function getPriceOptionIJ2(): float
    {
        return $this->priceOptionIJ2;
    }

    public function setPriceOptionIJ3(float $priceOptionIJ3): self
    {
        $this->priceOptionIJ3 = $priceOptionIJ3;

        return $this;
    }

    public function getPriceOptionIJ3(): float
    {
        return $this->priceOptionIJ3;
    }
}
