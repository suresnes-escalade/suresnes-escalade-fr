<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use App\Repository\SlotRepository;
use Stringable;
use LogicException;
use DateTimeInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Slot
 */
#[ORM\Entity(repositoryClass: SlotRepository::class)]
#[ORM\Table(name: 'wp_app_slot')]
class Slot implements Stringable
{
    #[ORM\Column(name: 'id', type: Types::INTEGER)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private int $id;

    #[Assert\NotBlank]
    #[ORM\ManyToOne(targetEntity: Season::class, cascade: ['all'], fetch: 'EAGER', inversedBy: 'slots')]
    private ?Season $season = null;

    #[Assert\NotBlank]
    #[ORM\Column(name: 'title', type: Types::STRING, length: 64, nullable: false)]
    private string $title;

    #[ORM\Column(name: 'description', type: Types::STRING, length: 255, nullable: true)]
    private ?string $description = null;

    #[Assert\Range(min: 1, max: 7)]
    #[ORM\Column(name: 'dayOfWeek', type: Types::INTEGER, columnDefinition: 'TINYINT', nullable: false)]
    private int $dayOfWeek;

    #[ORM\Column(name: 'beginTime', type: Types::TIME_MUTABLE, nullable: false)]
    private DateTimeInterface $beginTime;

    #[ORM\Column(name: 'endTime', type: Types::TIME_MUTABLE, nullable: false)]
    private DateTimeInterface $endTime;

    #[ORM\Column(name: 'minBirth', type: Types::DATE_MUTABLE, nullable: true)]
    private ?DateTimeInterface $minBirth = null;

    #[ORM\Column(name: 'maxBirth', type: Types::DATE_MUTABLE, nullable: true)]
    private ?DateTimeInterface $maxBirth = null;

    #[Assert\NotBlank]
    #[ORM\Column(name: 'maxPlaces', type: Types::INTEGER)]
    private int $maxPlaces = 20;

    #[Assert\NotBlank]
    #[ORM\Column(name: 'marginPlaces', type: Types::INTEGER)]
    private int $marginPlaces = 2;

    /**
     * @var Collection<int, Subscription> $subscriptions
     */
    #[ORM\OneToMany(targetEntity: Subscription::class, mappedBy: 'activatedSlot', cascade: ['persist', 'remove'], orphanRemoval: false)]
    #[ORM\OrderBy(['status' => 'DESC', 'firstName' => 'ASC', 'lastName' => 'ASC'])]
    private Collection $subscriptions;

    /**
     * @var Collection<int, Subscription> $waitingSubscriptions
     */
    #[ORM\OneToMany(targetEntity: Subscription::class, mappedBy: 'slot1', cascade: ['persist', 'remove'], orphanRemoval: false)]
    #[ORM\OrderBy(['status' => 'DESC', 'createdAt' => 'ASC'])]
    private Collection $waitingSubscriptions;

    #[ORM\Column(name: 'is_full', type: Types::BOOLEAN)]
    private bool $isFull;

    /**
     * @return array<int, string>
     */
    public static function getDayChoices()
    {
        return [
            1 => 'Lundi',
            2 => 'Mardi',
            3 => 'Mercredi',
            4 => 'Jeudi',
            5 => 'Vendredi',
            6 => 'Samedi',
            7 => 'Dimanche',
        ];
    }

    public function __construct()
    {
        $this->waitingSubscriptions = new ArrayCollection();
        $this->subscriptions = new ArrayCollection();
    }

    public function __toString(): string
    {
        $label = sprintf('%s / %s', $this->season, $this->title);

        if ($this->isFull) {
            $label = '[COMPLET] ' . $label;
        }

        return $label;
    }

    public function getLabel(): string
    {
        $label = $this->title;

        if ($this->isFull) {
            $label = '[COMPLET] ' . $label;
        }

        return $label;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setDayOfWeek(int $dayOfWeek): self
    {
        $this->dayOfWeek = $dayOfWeek;

        return $this;
    }

    public function getDayOfWeek(): int
    {
        return $this->dayOfWeek;
    }

    public function getReadableDayOfWeek(): string
    {
        return match ($this->dayOfWeek) {
            1 => 'Lundi',
            2 => 'Mardi',
            3 => 'Mercredi',
            4 => 'Jeudi',
            5 => 'Vendredi',
            6 => 'Samedi',
            7 => 'Dimanche',
            default => throw new LogicException('Incorrect day number: ' . $this->dayOfWeek),
        };
    }

    public function setBeginTime(DateTimeInterface $beginTime): self
    {
        $this->beginTime = $beginTime;

        return $this;
    }

    public function getBeginTime(): DateTimeInterface
    {
        return $this->beginTime;
    }

    public function setEndTime(DateTimeInterface $endTime): self
    {
        $this->endTime = $endTime;

        return $this;
    }

    public function getEndTime(): DateTimeInterface
    {
        return $this->endTime;
    }

    public function setMinBirth(DateTimeInterface $minBirth): self
    {
        $this->minBirth = $minBirth;

        return $this;
    }

    public function getMinBirth(): ?DateTimeInterface
    {
        return $this->minBirth;
    }

    public function setMaxBirth(DateTimeInterface $maxBirth): self
    {
        $this->maxBirth = $maxBirth;

        return $this;
    }

    public function getMaxBirth(): ?DateTimeInterface
    {
        return $this->maxBirth;
    }

    public function setSeason(Season $season = null): self
    {
        $this->season = $season;

        return $this;
    }

    public function getSeason(): ?Season
    {
        return $this->season;
    }

    public function setMaxPlaces(int $maxPlaces): self
    {
        $this->maxPlaces = $maxPlaces;

        return $this;
    }

    public function getMaxPlaces(): int
    {
        return $this->maxPlaces;
    }

    public function setMarginPlaces(int $marginPlaces): self
    {
        $this->marginPlaces = $marginPlaces;

        return $this;
    }

    public function getMarginPlaces(): int
    {
        return $this->marginPlaces;
    }

    public function addSubscription(Subscription $subscription): self
    {
        $this->subscriptions[] = $subscription;

        return $this;
    }

    public function removeSubscription(Subscription $subscription): void
    {
        $this->subscriptions->removeElement($subscription);
    }

    /**
     * @return Collection<int, Subscription>
     */
    public function getSubscriptions(): Collection
    {
        return $this->subscriptions;
    }

    public function setDescription(?string $description = null): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function addWaitingSubscription(Subscription $waitingSubscription): self
    {
        $this->waitingSubscriptions[] = $waitingSubscription;

        return $this;
    }

    public function removeWaitingSubscription(Subscription $waitingSubscription): void
    {
        $this->waitingSubscriptions->removeElement($waitingSubscription);
    }

    /**
     * @return Collection<int, Subscription>
     */
    public function getWaitingSubscriptions(): Collection
    {
        return $this->waitingSubscriptions;
    }

    public function setIsFull(bool $isFull): self
    {
        $this->isFull = $isFull;

        return $this;
    }

    public function getIsFull(): bool
    {
        return $this->isFull;
    }

    public function getIsFilled(): bool
    {
        return (count($this->subscriptions) >= $this->maxPlaces);
    }
}
