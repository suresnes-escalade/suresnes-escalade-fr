<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Event;

use App\Entity\Subscription;
use Symfony\Contracts\EventDispatcher\Event;

class SubscriptionEvent extends Event
{
    public function __construct(private readonly Subscription $subscription)
    {
    }

    public function getSubscription(): Subscription
    {
        return $this->subscription;
    }
}
