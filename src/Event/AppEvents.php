<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Event;

final class AppEvents
{
    public const SUBSCRIPTION_UPDATED = 'app.subscription.updated';
    public const SUBSCRIPTION_SUBMITTED = 'app.subscription.submitted';
    public const SUBSCRIPTION_VALIDATED = 'app.subscription.validated';
    public const SUBSCRIPTION_CANCELLED = 'app.subscription.cancelled';

    private function __construct()
    {
    }
}
