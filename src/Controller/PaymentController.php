<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Controller;

use App\Entity\Payment;
use App\Repository\PaymentRepository;
use stdClass;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class PaymentController extends AbstractController
{
    #[Route(path: '%hello_route_new_payment%')]
    public function helloasso(Request $request, PaymentRepository $paymentRepository): Response
    {
        if ($request->isMethod('POST')) {
            /** @var stdClass $json */
            $json = json_decode($request->getContent(), false);

            $payment = new Payment();
            $payment->fromJson($json);
            $paymentRepository->addOrUpdate($payment);

            return new JsonResponse(['No content'], Response::HTTP_NO_CONTENT);
        }

        return new JsonResponse(['Error'], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
