<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Controller\Admin;

use App\Entity\Subscription;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @extends CRUDController<Subscription>
 */
class SubscriptionAdminController extends CRUDController
{
    public function printAction(int $id): Response
    {
        $subscription = $this->admin->getObject($id);

        if (!$subscription) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if ($this->admin->isGranted('EDIT', $subscription) === false) {
            throw new AccessDeniedException();
        }

        $this->admin->setSubject($subscription);

        return $this->render('simple_subscription/print.html.twig', ['subscription' => $subscription]);
    }

    public function voucherAction(int $id): Response
    {
        $subscription = $this->admin->getObject($id);

        if (!$subscription) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if ($this->admin->isGranted('EDIT', $subscription) === false) {
            throw new AccessDeniedException();
        }

        $this->admin->setSubject($subscription);

        return $this->render('simple_subscription/voucher.html.twig', ['subscription' => $subscription]);
    }
}
