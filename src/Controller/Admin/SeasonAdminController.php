<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Controller\Admin;

use App\Entity\Season;
use Symfony\Component\HttpFoundation\Response;
use DateTime;
use RuntimeException;
use DateInterval;
use Sonata\AdminBundle\Controller\CRUDController;
use App\Entity\Subscription;
use App\Model\SearchModel;
use App\Repository\SeasonRepository;
use App\Repository\SlotRepository;
use App\Repository\SubscriptionRepository;
use Exception;
use Psr\Log\LoggerInterface;
use Sonata\Exporter\Writer\CsvWriter;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @extends CRUDController<Season>
 */
class SeasonAdminController extends CRUDController
{
    public function __construct(private mixed $import_maping = [])
    {
    }

    public function exportFFMEAction(int $id, SubscriptionRepository $subscriptionRepository): Response
    {
        $season = $this->admin->getObject($id);

        if (!$season) {
            throw new NotFoundHttpException(sprintf('Unable to find season with id : %s', $id));
        }

        if ($this->admin->isGranted('EDIT', $season) === false) {
            throw new AccessDeniedException();
        }

        $this->admin->setSubject($season);

        $subscriptions = $subscriptionRepository->findBy([
            'season' => $season,
            'status' => Subscription::STATUS_ACTIVE,
            'licenceDate' => null,
        ], [
            'activatedAt' => 'ASC',
            'createdAt' => 'ASC',
        ]);

        $writer = new CsvWriter('php://output', ';', '"', '\\', true, true);

        $callback = function () use ($subscriptions, $writer): void {
            $writer->open();

            $f = fn ($phone) => preg_replace('/[^0-9]/', '', $phone);

            $insurances = [0 => 'RC', 1 => 'B', 2 => 'B+', 3 => 'B++'];
            $insurancesIj = [0 => 'NON', 1 => 'IJ1', 2 => 'IJ2', 3 => 'IJ3'];
            foreach ($subscriptions as $s) {
                $writer->write([
                    'ACTION' => $s->getLicence() ? 'R' : 'C', // Action  C, R ou MAJ
                    'NOM' => substr((string) $s->getLastName(), 0, 100), // Nom 100 caractères maximum
                    'PRENOM' => substr((string) $s->getFirstName(), 0, 100), // Prénom  100 caractères maximum
                    'DATE DE NAISSANCE' => $s->getBirthday() ? $s->getBirthday()->format('d/m/Y') : '', // Date naissance  format : jj/mm/aaaa
                    'SEXE' => $s->getSex() == 'F' ? 'F' : 'H', // Sexe    H ou F
                    'NATIONALITE' => $s->getNationality() ?: 'FR', // Nationalité 2 caractères (voir tableau des codes pays)
                    'ADRESSE' => substr((string) $s->getAddress(), 0, 255), // Adresse 255 caractères maximum
                    'ADRESSE COMPLEMENT' => substr((string) $s->getAddress(), 255, 255 * 2), // Complément d'adresse    255 caractères maximum
                    'CODE POSTAL' => $s->getPostalCode(), // Code postal Si Pays=France, code postal de 5 chiffres
                    'VILLE' => $s->getCity(), // Ville   100 caractères maximum
                    'PAYS' => 'FR', // Pays    2 caractères (voir tableau des codes pays)
                    'TEL FIXE' => $f($s->getPhone()), // Téléphone   Si Pays=France, 10 chiffres
                    'TEL MOBILE' => $f($s->getPhone()), // Mobile  Si Pays=France, 10 chiffres  (au moins 1 des 2)
                    'COURRIEL' => $s->getEmail(), // Courriel    100 caractères maximum
                    'COURRIEL 2' => $s->getParentEmail(),  // Courriel  Parent
                    'GROUPE' => $s->getActivatedSlot() ? $s->getActivatedSlot()->getLabel() : '', // Champ libre pour affecter un licencié à un groupe
                    'PAP NOM' => substr((string) $s->getParentLastName(), 0, 100), // Personne à prévenir - Nom   100 caractères maximum
                    'PAP PRENOM' => substr((string) $s->getParentFirstName(), 0, 100), // Personne à prévenir - Prénom    100 caractères maximum
                    'PAP ADRESSE' => '', // Personne à prévenir - Adresse   255 caractères maximum
                    'PAP ADRESSE COMPLEMENT' => '', // Personne à prévenir - Compl. d'adresse  255 caractères maximum
                    'PAP CODE POSTAL' => '', // Personne à prévenir - Code postal   10 caractères maximum
                    'PAP VILE' => '', // TYPO ?? Personne à prévenir - Ville 100 caractères maximum
                    'PAP TELEPHONE' => $f($s->getParentPhone()), // Personne à prévenir - Téléphone 15 caractères maximum
                    'PAP COURRIEL' => $s->getParentEmail(), // Personne à prévenir - Courriel  100 caractères maximum
                    'NUMERO DE LICENCE' => $s->getLicence(), // N° licence  6 chiffres  AUTOMATIQUE
                    'TYPE LICENCE' => $s->getAge() < 18 ? 'J' : 'A', // Type licence    J, A ou F
                    'ASSURANCE' => $insurances[$s->getInsurance()], // Assurance   RC, B, B+ ou B++
                    'OPTION SKI' => $s->getInsuranceSki() ? 'OUI' : 'NON', // Option ski  OUI ou NON
                    'OPTION SLACKLINE' => $s->getInsuranceSlackline() ? 'OUI' : 'NON', // Option Slackline    OUI ou NON
                    'OPTION TRAIL' => 'NON', // Option Trail    OUI ou NON
                    'OPTION VTT' => 'NON', // Option VTT  OUI ou NON
                    'ASSURANCE COMPLEMENTAIRE' => $insurancesIj[$s->getInsuranceIj()], // Option assurance    IJ1, IJ2, IJ3 ou NON
                    //'CM DATE' => $s->getCertificateDate()->format('d/m/Y'), // Certificat médical - Date   format : jj/mm/aaaa
                    'CM ATTESTATION SANTE' => $s->getCertificateOld() ? 'OUI' : 'NON', // ATTESTATION FOURNIE    OUI ou NON
                    'CM TYPE' => $s->getCertificateOld() ? '' : ($s->getCertificateCompetition() ? 'C' : 'L'), // Certificat médical - Type   L ou C ou vide si CM ATTESTATION SANTE=OUI
                    'CM MEDECIN' => $s->getCertificateOld() ? '' : (substr((string) $s->getCertificateDoctor(), 0, 100)), // Certificat médical - Médecin    100 caractères maximum ou vide si CM ATTESTATION SANTE=OUI
                    'DIFFUSION COORDONNEES' => 'NON', // Diffusion des coordonnées   OUI ou NON
                    'ABONNEMENT DIRECT INFO' => 'NON', // Abonnement Direct Info  OUI ou NON
                    'ABO MAGAZINE EN LIGNE' => 'OUI', // Abonnement magazine en ligne gratuit    OUI ou NON
                ]);
            }
            $writer->close();
        };

        return new StreamedResponse($callback, Response::HTTP_OK, [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => sprintf('attachment; filename=%s', 'suresnes-escalade-' . date('Y-m-d') . '.csv')
        ]);
    }

    public function importFFMEAction(int $id, Request $request, SubscriptionRepository $subscriptionRepository, LoggerInterface $logger): Response
    {
        $season = $this->admin->getObject($id);

        if (!$season) {
            throw new NotFoundHttpException(sprintf('Unable to find the season with id : %s', $id));
        }

        if ($this->admin->isGranted('EDIT', $season) === false) {
            throw new AccessDeniedException();
        }

        $form = $this->createFormBuilder()
            ->add('file', FileType::class)
            ->add('import', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $form['file']) {
            /** @var UploadedFile $csv_file */
            $csv_file = $form['file']->getData();
            $f = fopen($csv_file->getPathname(), 'r');

            if (!$f) {
                throw new Exception('Unable to open csv file');
            }

            // Progress file pointer and get first 3 characters to compare to the BOM string.
            if (fgets($f, 4) !== "\xef\xbb\xbf") {
                // BOM not found - rewind pointer to start of file.
                rewind($f);
            }

            $head = fgetcsv($f, 0, $this->import_maping['separator'], '"');
            if (!is_array($head)) {
                $this->addFlash('sonata_flash_error', 'Format de fichier incorrect.');
                $this->render('season_admin/importFFME.html.twig', [
                    'form' => $form->createView(),
                    'action' => 'importFFME',
                    'object' => $season,
                ]);
            }

            if ($head === false) {
                throw new Exception('Format de fichier incorrect');
            }

            /** @phpstan-ignore-next-line */
            $head = array_flip($head);

            /*
              8 : Nom
              9 : Prénom
              11: Date de naissance
              10: Civilité
              0 : N° de licence
              1 : TYPE LICENCE
              2 : ASSURANCE
              3 : ASSURANCE OPTION SKI
              4 : ASSURANCE OPTION SLACKLINE ET HIGHLINE
              5 : ASSURANCE OPTION TRAIL
              6 : ASSURANCE OPTION VTT
              7 : ASSURANCE COMPLEMENTAIRE
              12: ADRESSE1
              13: ADRESSE2
              14: C.POSTE
              15: VILLE
              16: TEL.
              17: MOBILE
              18: FAX
              19: EMAIL
              20: REVUE
              21: STRUCTURE
              22: CERTIFICAT.MEDICAL
              23: MEDECIN
              24: DATE CERTIFICAT
              25: DATE.MODIFICATION
              26: ACCIDENT_CONTACT
              27: ACCIDENT_ADRESSE1
              28: ACCIDENT_ADRESSE2
              29: ACCIDENT_CP
              30: ACCIDENT_VILLE
              31: ACCIDENT_TEL
              32: ACTIVITE
              33: DIPLOME
              34: CODE.UTILISATEUR
              35: ENVOI MAIL
              36: ENVOI SMS
              37: DATE CREATION LICENCE
             */

            $missingCols = [];
            $mandatoryCols = array_values($this->import_maping['fields']);
            foreach ($mandatoryCols as $mandatoryCol) {
                if (!isset($head[$mandatoryCol])) {
                    $missingCols[] = $mandatoryCol;
                }
            }
            if ($missingCols !== []) {
                $this->addFlash(
                    'sonata_flash_error',
                    sprintf(
                        'Format de fichier incorrect. Les colonnes obligatoire "%s" ne sont pas toutes présentes dans le fichier. Les colonnes manquantes sont : %s',
                        implode(', ', $mandatoryCols),
                        implode(', ', $missingCols)
                    )
                );
                return $this->render('season_admin/importFFME.html.twig', [
                    'form' => $form->createView(),
                    'action' => 'importFFME',
                    'object' => $season,
                ]);
            }

            $values = null;
            $num = 0;
            $searchParams = new SearchModel();

            try {
                while (($values = fgetcsv($f, 0, $this->import_maping['separator'], '"')) && !feof($f)) {
                    $num++;

                    $searchParams->setSeason($season);

                    if ($values[$head[$this->import_maping['fields']['birthday']]] && $birthDay = DateTime::createFromFormat('d/m/Y', $values[$head[$this->import_maping['fields']['birthday']]])) {
                        $searchParams->setBirthday($birthDay);
                    }

                    if ($values[$head[$this->import_maping['fields']['firstname']]]) {
                        $searchParams->setFirstname(strtr($values[$head[$this->import_maping['fields']['firstname']]], ' ', '_'));
                    }

                    if ($values[$head[$this->import_maping['fields']['lastname']]]) {
                        $searchParams->setLastname(strtr($values[$head[$this->import_maping['fields']['lastname']]], ' ', '_'));
                    }

                    $subscription = $subscriptionRepository->searchOneActivedForSeasonAndOtherParams($searchParams);

                    if (!$subscription instanceof Subscription) {
                        throw new RuntimeException('Impossible de trouver l\'adhérent');
                    }

                    if ($subscription->getLicence() && $subscription->getLicence() !== $values[$head[$this->import_maping['fields']['licence']]]) {
                        throw new RuntimeException('Le numéro de licence ' . $subscription->getLicence() . ' ne correspond pas');
                    }

                    if ($values[$head[$this->import_maping['licence_date']]] && $licenceDate = DateTime::createFromFormat('d/m/Y', $values[$head[$this->import_maping['licence_date']]])) {
                        $subscription->setLicenceDate($licenceDate);
                    }

                    $subscriptionRepository->addOrUpdate($subscription);
                }

                $this->addFlash('sonata_flash_success', 'Fichier importé avec succès. ' . $num . ' adhérents ont leur licence FFME à jour.');
            } catch (RuntimeException $e) {
                $logger->error('{exception}', ['exception' => $e, 'values' => $values]);
                if (!empty($values)) {
                    $this->addFlash(
                        'sonata_flash_error',
                        'Ligne ' . $num . ' : ' . $e->getMessage() . ' ' . $values[$head[$this->import_maping['fields']['firstname']]] . ' ' . $values[$head[$this->import_maping['fields']['lastname']]] . ' ' .
                        ($searchParams->getBirthday() != null ? $searchParams->getBirthday()->format('Y-m-d') : '') . ' (' . $values[$head[$this->import_maping['fields']['licence']]] . ')'
                    );
                }

                return $this->render('season_admin/importFFME.html.twig', [
                    'form' => $form->createView(),
                    'action' => 'importFFME',
                    'object' => $season,
                ]);
            }
        }

        return $this->render('season_admin/importFFME.html.twig', [
            'form' => $form->createView(),
            'action' => 'importFFME',
            'object' => $season,
        ]);
    }

    public function cloneAction(Request $request, int $id, SlotRepository $slotRepository, SeasonRepository $seasonRepository): Response
    {
        /** @var ?Season $season */
        $season = $this->admin->getObject($id);

        if (!$season) {
            throw new NotFoundHttpException(sprintf('unable to find season with id : %s', $id));
        }

        if ($this->admin->isGranted('NEW', $season) === false) {
            throw new AccessDeniedException();
        }

        // Clone the entity
        foreach ($season->getSlots() as $slot) {
            $slotRepository->addSlot($slot);
        }

        // Add 1 year
        $oneYear = new DateInterval('P1Y');
        /** @var DateTime $beginAt */
        $beginAt = $season->getBeginAt();
        $beginAt->add($oneYear);
        /** @var DateTime $endAt */
        $endAt = $season->getEndAt();
        $endAt->add($oneYear);
        $season->setTitle(sprintf('Saison %d-%d', $beginAt->format('Y'), $endAt->format('Y')));

        // Reset slots
        foreach ($season->getSlots() as $slot) {
            $slot->setIsFull(false);
        }

        $this->admin->create($season);

        $this->addFlash('sonata_flash_success', $this->trans('flash_create_success', ['%name%' => $this->admin->toString($season)], 'SonataAdminBundle'));

        return $this->redirectTo($request, $season);
    }
}
