<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Subscription;
use App\Event\AppEvents;
use App\Event\SubscriptionEvent;
use App\Form\SimpleSubscriptionType;
use App\Repository\SeasonRepository;
use App\Repository\SlotRepository;
use App\Repository\SubscriptionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Routing\Attribute\Route;

/**
 * Subscription controller.
 */
#[Route(path: '/simple')]
class SimpleSubscriptionController extends AbstractController
{
    public const CURRENT_SEASON = true;
    public const ANY_SEASON = false;

    public function __construct(private readonly SeasonRepository $seasonRepository, private readonly SlotRepository $slotRepository, private readonly SubscriptionRepository $subscriptionRepository)
    {
    }

    #[Route(path: '/inscription', name: 'simple_subscription')]
    public function form(Request $request, EventDispatcherInterface $dispatcher): Response
    {
        if ($request->query->has('new')) {
            $request->getSession()->remove('SUBSCRIPTION');

            return $this->redirectToRoute('simple_subscription');
        }

        $subscription = $this->findPrivateSubscription($request, static::CURRENT_SEASON);

        if ($subscription->isActive()) {
            return $this->redirectToRoute('simple_subscription_accepted');
        }

        $form = $this->createForm(
            SimpleSubscriptionType::class,
            $subscription,
            [
                'action' => $this->generateUrl('simple_subscription'),
                'currentSeason' => $subscription->getSeason()
            ]
        );
        $form->add('submit', SubmitType::class, ['label' => 'Valider']);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$subscription->getNoSlot() && $subscription->getSlot1() && $subscription->getSlot1()->getIsFilled() && $subscription->getSlot2() && $subscription->getSlot2()->getIsFilled()) {
                $subscription->setStatus(Subscription::STATUS_WAITING);
            } else {
                $subscription->setStatus(Subscription::STATUS_PAYMENT_PENDING);
            }
            $this->subscriptionRepository->addOrUpdate($subscription);

            $request->getSession()->set('SUBSCRIPTION', $subscription->getId() . '/' . $subscription->getKey());

            $dispatcher->dispatch(new SubscriptionEvent($subscription), AppEvents::SUBSCRIPTION_SUBMITTED);

            return $this->redirectToRoute('simple_subscription_submitted');
        }

        return $this->render('simple_subscription/form.html.twig', ['subscription' => $subscription, 'form' => $form->createView()]);
    }

    #[Route(path: '/confirmation', name: 'simple_subscription_submitted')]
    public function submitted(Request $request): Response
    {
        $subscription = $this->findPrivateSubscription($request, static::CURRENT_SEASON);

        if (!$subscription->getId()) {
            throw $this->createNotFoundException();
        }

        return $this->render('simple_subscription/submitted.html.twig', ['subscription' => $subscription]);
    }

    #[Route(path: '/inscription-acceptee', name: 'simple_subscription_accepted')]
    public function accepted(Request $request): Response
    {
        $subscription = $this->findPrivateSubscription($request, static::ANY_SEASON);

        if (!$subscription->getId()) {
            throw $this->createNotFoundException();
        }

        if (!$subscription->isActive()) {
            return $this->redirectToRoute('simple_subscription');
        }

        return $this->render('simple_subscription/accepted.html.twig', ['subscription' => $subscription]);
    }

    #[Route(path: '/inscription/imprimer', name: 'simple_subscription_print')]
    public function print(Request $request): Response
    {
        $subscription = $this->findPrivateSubscription($request, static::CURRENT_SEASON);

        if (!$subscription->getId()) {
            throw $this->createNotFoundException();
        }

        return $this->render('simple_subscription/print.html.twig', ['subscription' => $subscription]);
    }

    #[Route(path: '/inscription-justificatif', name: 'simple_subscription_voucher')]
    public function voucher(Request $request): Response
    {
        $subscription = $this->findPrivateSubscription($request, static::ANY_SEASON);

        if (!$subscription->getId()) {
            throw $this->createNotFoundException();
        }

        return $this->render('simple_subscription/voucher.html.twig', ['subscription' => $subscription]);
    }

    private function findPrivateSubscription(Request $request, bool $current = true): Subscription
    {
        $session = $request->getSession();
        $ref = $key = null;

        if ($request->query->has('ref')) {
            $ref = $request->query->get('ref');
            $key = $request->query->get('key');
        } elseif ($session->has('SUBSCRIPTION')) {
            /** @var string $session_subscription */
            $session_subscription = $session->get('SUBSCRIPTION');
            [$ref, $key] = explode('/', $session_subscription, 2);
        }

        $season = $this->seasonRepository->findCurrent();
        $slots = $this->slotRepository->findForSeason($season);
        foreach ($slots as $slot) {
            $season->addSlot($slot);
        }

        $subscription = null;

        if ($key && $ref) {
            $criteria = [
                'id' => $ref,
                'key' => $key,
            ];

            if (static::CURRENT_SEASON === $current) {
                $criteria['season'] = $season;
            }

            $subscription = $this->subscriptionRepository->findOneBy($criteria);
        }

        if ($subscription) {
            $session->set('SUBSCRIPTION', $ref . '/' . $key);
        } else {
            $session->remove('SUBSCRIPTION');

            $subscription = new Subscription();
            $subscription->setSeason($season);
            $subscription->setIsRenewal(false);
        }

        return $subscription;
    }
}
