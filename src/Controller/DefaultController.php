<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Controller;

use App\Kernel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class DefaultController extends AbstractController
{
    #[Route(path: '/version', name: 'app_version')]
    public function version(): Response
    {
        $tpl_params = ['version' => [
            'symfony' => Kernel::VERSION,
            'php' => \PHP_VERSION,
        ]];

        return $this->render('_version.html.twig', $tpl_params);
    }
}
