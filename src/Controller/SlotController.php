<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use App\Repository\SeasonRepository;
use App\Repository\SlotRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;

/**
 * Subscription controller.
 */
#[Route(path: '/creneaux')]
class SlotController extends AbstractController
{
    /**
     * Lists all Subscription entities.
     */
    #[Route(path: '/', name: 'slots', methods: ['GET'])]
    public function index(SeasonRepository $seasonRepository): Response
    {
        $season = $seasonRepository->findCurrent();

        return $this->render('slot/index.html.twig', ['season' => $season]);
    }

    /**
     * Lists all Subscription entities.
     */
    #[Route(path: '/inscrits', name: 'slots_subscriptions', methods: ['GET'])]
    #[Route(path: '/details', name: 'slots_details', methods: ['GET'])]
    public function subscriptions(SeasonRepository $seasonRepository, SlotRepository $slotRepository, Request $request): Response
    {
        $season = $seasonRepository->findCurrent();
        $slots = $slotRepository->findForSeason($season);
        $routeName = $request->attributes->get('_route');

        return $this->render('slot/subscriptions.html.twig', [
            'season' => $season,
            'slots' => $slots,
            'full' => $routeName === 'slots_details'
        ]);
    }
}
