<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Repository;

use DateTime;
use App\Entity\Season;
use App\Entity\Subscription;
use App\Model\SearchModel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Subscription>
 */
class SubscriptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Subscription::class);
    }

    public function addOrUpdate(Subscription $subscription): ?int
    {
        $this->_em->persist($subscription);
        $this->_em->flush();

        return $subscription->getId();
    }

    /**
     * @return Subscription[]
     */
    public function findActivedForSeason(Season $season): array
    {
        $q = $this->createQueryBuilder('s')
            ->where('s.season = :season')
            ->andWhere('s.status = :status')
            ->setParameter('season', $season)
            ->setParameter('status', Subscription::STATUS_ACTIVE);

        return $q->getQuery()->getResult();
    }

    public function countForCurrentSeason(?int $status = null): int
    {
        $date = new DateTime();

        $q = $this->createQueryBuilder('s')
            ->select('count(s.id)')
            ->leftJoin('s.season', 'season')
            ->where('season.beginAt < :date')
            ->andWhere('season.endAt > :date')
            ->setParameter('date', $date->format('Y-m-d'));

        if ($status !== null) {
            $q
                ->andWhere('s.status = :status')
                ->setParameter('status', $status);
        }

        return (int) $q->getQuery()->getSingleScalarResult();
    }

    public function searchOneActivedForSeasonAndOtherParams(SearchModel $searchParams): ?Subscription
    {
        $q = $this->createQueryBuilder('s')
            ->where('s.season = :season')
            ->andWhere('s.firstName LIKE :firstname')
            ->andWhere('s.lastName LIKE :lastname')
            ->andWhere('s.birthday = :birthday')
            ->andWhere('s.status = :status')
            ->setParameter('season', $searchParams->getSeason())
            ->setParameter('firstname', '%' . $searchParams->getFirstname() . '%')
            ->setParameter('lastname', '%' . $searchParams->getLastname() . '%')
            ->setParameter('birthday', ($searchParams->getBirthday() != null ? $searchParams->getBirthday()->format('Y-m-d') : ''))
            ->setParameter('status', Subscription::STATUS_ACTIVE)
            ->setMaxResults(1);

        return $q->getQuery()->getOneOrNullResult(Query::HYDRATE_OBJECT);
    }
}
