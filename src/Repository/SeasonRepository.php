<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Repository;

use DateTime;
use RuntimeException;
use App\Entity\Season;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Season>
 */
class SeasonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Season::class);
    }

    public function addSeason(Season $season): ?int
    {
        $this->_em->persist($season);
        $this->_em->flush();

        return $season->getId();
    }

    public function findCurrent(DateTime $date = null): Season
    {
        if (!$date instanceof DateTime) {
            $date = new DateTime();
        }

        $q = $this->createQueryBuilder('s')
            ->where('s.beginAt < :date')
            ->andWhere('s.endAt > :date')
            ->setMaxResults(1)
            ->orderBy('s.endAt', 'DESC')
            ->setParameter('date', $date->format('Y-m-d'));

        $season = $q
            ->getQuery()
            ->getOneOrNullResult(Query::HYDRATE_OBJECT);

        if (!$season) {
            throw new RuntimeException('No season found');
        }

        return $season;
    }
}
