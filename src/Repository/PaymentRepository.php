<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Repository;

use App\Entity\Payment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Payment>
 */
class PaymentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Payment::class);
    }

    public function addOrUpdate(Payment $payment): ?int
    {
        $this->_em->persist($payment);
        $this->_em->flush();

        return $payment->getId();
    }
}
