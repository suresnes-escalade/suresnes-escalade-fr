<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Repository;

use DateTime;
use App\Entity\Season;
use App\Entity\Slot;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Slot>
 */
class SlotRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Slot::class);
    }

    public function addSlot(Slot $slot): ?int
    {
        $this->_em->persist($slot);
        $this->_em->flush();

        return $slot->getId();
    }

    /**
     * @return Slot[]
     */
    public function findForSeason(Season $season): array
    {
        $q = $this->createQueryBuilder('s')
            ->leftJoin('s.waitingSubscriptions', 'ws')
            ->addSelect('ws')
            ->leftJoin('s.subscriptions', 'sub')
            ->addSelect('sub')
            ->where('s.season = :season')
            ->setParameter('season', $season)
            ->orderBy('s.title', 'ASC');

        return $q->getQuery()->getResult();
    }

    public function getSlotsQueryForSeason(Season $season): QueryBuilder
    {
        return $this->createQueryBuilder('s')
            ->leftJoin('s.waitingSubscriptions', 'ws')
            ->addSelect('ws')
            ->leftJoin('s.subscriptions', 'sub')
            ->addSelect('sub')
            ->where('s.season = :season')
            ->setParameter('season', $season)
            ->orderBy('s.title', 'ASC');
    }

    public function countForCurrentSeason(): int
    {
        $date = new DateTime();

        $q = $this->createQueryBuilder('s')
            ->select('count(s.id)')
            ->leftJoin('s.season', 'season')
            ->where('season.beginAt < :date')
            ->andWhere('season.endAt > :date')
            ->setParameter('date', $date->format('Y-m-d'));

        return (int) $q->getQuery()->getSingleScalarResult();
    }
}
