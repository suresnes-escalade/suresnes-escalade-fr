<?php
/*
 * This file is part of suresnes-escalade website
 */

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<User>
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function addUser(User $user): int
    {
        $this->_em->persist($user);
        $this->_em->flush();

        return $user->getId();
    }

    public function findOneByUsernameOrEmail(string $username_or_email): ?User
    {
        $qb = $this->createQueryBuilder('u');
        $qb->leftJoin('u.metas', 'm');
        $qb->where('u.login = :identifier');
        $qb->orWhere('u.email = :identifier');
        $qb->setParameter('identifier', $username_or_email);

        return $qb->getQuery()->getOneOrNullResult(Query::HYDRATE_OBJECT);
    }
}
